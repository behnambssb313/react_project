"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _localeUtils = _interopRequireDefault(require("../shared/localeUtils"));

var _generalUtils = require("../shared/generalUtils");

var _sliderHelpers = require("../shared/sliderHelpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var Header = function Header(_ref) {
  var maximumDate = _ref.maximumDate,
      minimumDate = _ref.minimumDate,
      onMonthChange = _ref.onMonthChange,
      activeDate = _ref.activeDate,
      monthChangeDirection = _ref.monthChangeDirection,
      onMonthSelect = _ref.onMonthSelect,
      onYearSelect = _ref.onYearSelect,
      isMonthSelectorOpen = _ref.isMonthSelectorOpen,
      isYearSelectorOpen = _ref.isYearSelectorOpen,
      isPersian = _ref.isPersian;
  var headerElement = (0, _react.useRef)(null);
  var monthYearWrapperElement = (0, _react.useRef)(null);

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getMonthName = _useMemo.getMonthName,
      isBeforeDate = _useMemo.isBeforeDate,
      getLanguageDigits = _useMemo.getLanguageDigits;

  (0, _react.useEffect)(function () {
    if (!monthChangeDirection) return;
    (0, _sliderHelpers.animateContent)({
      direction: monthChangeDirection,
      parent: monthYearWrapperElement.current
    });
  }, [monthChangeDirection]);
  (0, _react.useEffect)(function () {
    var isOpen = isMonthSelectorOpen || isYearSelectorOpen;
    var monthText = headerElement.current.querySelector('.Calendar__monthYear.-shown .Calendar__monthText');
    var yearText = monthText.nextSibling;

    var hasActiveBackground = function hasActiveBackground(element) {
      return element.classList.contains('-activeBackground');
    };

    var isInitialRender = !isOpen && !hasActiveBackground(monthText) && !hasActiveBackground(yearText);
    if (isInitialRender) return;

    var arrows = _toConsumableArray(headerElement.current.querySelectorAll('.Calendar__monthArrowWrapper'));

    var hasMonthSelectorToggled = isMonthSelectorOpen || hasActiveBackground(monthText);
    var primaryElement = hasMonthSelectorToggled ? monthText : yearText;
    var secondaryElement = hasMonthSelectorToggled ? yearText : monthText;
    var translateXDirection = hasMonthSelectorToggled ? 1 : -1;
    if (isPersian) translateXDirection *= -1;
    var scale = !isOpen ? 1 : 1.05;
    var translateX = !isOpen ? 0 : "".concat(translateXDirection * secondaryElement.offsetWidth / 2);
    secondaryElement.style.transform = '';
    primaryElement.style.transform = "scale(".concat(scale, ") ").concat(translateX ? "translateX(".concat(translateX, "px)") : '');
    primaryElement.classList.toggle('-activeBackground');
    secondaryElement.classList.toggle('-hidden');
    arrows.forEach(function (arrow) {
      arrow.classList.toggle('-hidden');
    });
  }, [isMonthSelectorOpen, isYearSelectorOpen]);

  var getMonthYearText = function getMonthYearText(isInitialActiveChild) {
    var date = (0, _sliderHelpers.getSlideDate)({
      isInitialActiveChild: isInitialActiveChild,
      monthChangeDirection: monthChangeDirection,
      activeDate: activeDate,
      parent: monthYearWrapperElement.current
    });
    var year = getLanguageDigits(date.year);
    var month = getMonthName(date.month);
    return {
      month: month,
      year: year
    };
  };

  var isNextMonthArrowDisabled = maximumDate && isBeforeDate(maximumDate, _objectSpread({}, activeDate, {
    month: activeDate.month + 1,
    day: 1
  }));
  var isPreviousMonthArrowDisabled = minimumDate && (isBeforeDate(_objectSpread({}, activeDate, {
    day: 1
  }), minimumDate) || (0, _generalUtils.isSameDay)(minimumDate, _objectSpread({}, activeDate, {
    day: 1
  })));

  var onMonthChangeTrigger = function onMonthChangeTrigger(direction) {
    var isMonthChanging = Array.from(monthYearWrapperElement.current.children).some(function (child) {
      return child.classList.contains('-shownAnimated');
    });
    if (isMonthChanging) return;
    onMonthChange(direction);
  };

  return _react["default"].createElement("div", {
    ref: headerElement,
    className: "Calendar__header"
  }, _react["default"].createElement("button", {
    tabIndex: "-1",
    className: "Calendar__monthArrowWrapper -right",
    onClick: function onClick() {
      onMonthChangeTrigger('PREVIOUS');
    },
    "aria-label": isPersian ? 'ماه قبل' : 'previous month',
    type: "button",
    disabled: isPreviousMonthArrowDisabled
  }, _react["default"].createElement("span", {
    className: "Calendar__monthArrow"
  }, "\xA0")), _react["default"].createElement("div", {
    className: "Calendar__monthYearContainer",
    ref: monthYearWrapperElement,
    "data-testid": "month-year-container"
  }, "\xA0", _react["default"].createElement("div", {
    onAnimationEnd: _sliderHelpers.handleSlideAnimationEnd,
    className: "Calendar__monthYear -shown"
  }, _react["default"].createElement("button", {
    tabIndex: "-1",
    onClick: onMonthSelect,
    type: "button",
    className: "Calendar__monthText"
  }, getMonthYearText(true).month), _react["default"].createElement("button", {
    tabIndex: "-1",
    onClick: onYearSelect,
    type: "button",
    className: "Calendar__yearText"
  }, getMonthYearText(true).year)), _react["default"].createElement("div", {
    onAnimationEnd: _sliderHelpers.handleSlideAnimationEnd,
    className: "Calendar__monthYear -hiddenNext"
  }, _react["default"].createElement("button", {
    tabIndex: "-1",
    onClick: onMonthSelect,
    type: "button",
    className: "Calendar__monthText"
  }, getMonthYearText(false).month), _react["default"].createElement("button", {
    tabIndex: "-1",
    onClick: onYearSelect,
    type: "button",
    className: "Calendar__yearText"
  }, getMonthYearText(false).year))), _react["default"].createElement("button", {
    tabIndex: "-1",
    className: "Calendar__monthArrowWrapper -left",
    onClick: function onClick() {
      onMonthChangeTrigger('NEXT');
    },
    "aria-label": isPersian ? 'ماه بعد' : 'next month',
    type: "button",
    disabled: isNextMonthArrowDisabled
  }, _react["default"].createElement("span", {
    className: "Calendar__monthArrow"
  }, "\xA0")));
};

var _default = Header;
exports["default"] = _default;