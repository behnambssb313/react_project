"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _localeUtils = _interopRequireDefault(require("../shared/localeUtils"));

var _constants = require("../shared/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

var YearSelector = function YearSelector(_ref) {
  var isOpen = _ref.isOpen,
      activeDate = _ref.activeDate,
      onYearSelect = _ref.onYearSelect,
      selectorStartingYear = _ref.selectorStartingYear,
      selectorEndingYear = _ref.selectorEndingYear,
      maximumDate = _ref.maximumDate,
      minimumDate = _ref.minimumDate,
      isPersian = _ref.isPersian;
  var wrapperElement = (0, _react.useRef)(null);
  var yearListElement = (0, _react.useRef)(null);

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getLanguageDigits = _useMemo.getLanguageDigits,
      getToday = _useMemo.getToday;

  (0, _react.useEffect)(function () {
    var classToggleMethod = isOpen ? 'add' : 'remove';
    var activeSelectorYear = wrapperElement.current.querySelector('.Calendar__yearSelectorItem.-active');
    wrapperElement.current.classList[classToggleMethod]('-faded');
    yearListElement.current.scrollTop = activeSelectorYear.offsetTop - activeSelectorYear.offsetHeight * 5;
    yearListElement.current.classList[classToggleMethod]('-open');
  }, [isOpen]);

  var renderSelectorYears = function renderSelectorYears() {
    var items = [];

    var startingYearValue = selectorStartingYear || getToday().year - _constants.MINIMUM_SELECTABLE_YEAR_SUBTRACT;

    var endingYearValue = selectorEndingYear || getToday().year + _constants.MAXIMUM_SELECTABLE_YEAR_SUM;

    for (var i = startingYearValue; i <= endingYearValue; i += 1) {
      items.push(i);
    }

    return items.map(function (item) {
      var isAfterMaximumDate = maximumDate && item > maximumDate.year;
      var isBeforeMinimumDate = minimumDate && item < minimumDate.year;
      return _react["default"].createElement("div", {
        key: item,
        className: "Calendar__yearSelectorItem ".concat(activeDate.year === item ? '-active' : '')
      }, _react["default"].createElement("button", {
        tabIndex: "-1",
        className: "Calendar__yearSelectorText",
        type: "button",
        onClick: function onClick() {
          onYearSelect(item);
        },
        disabled: isAfterMaximumDate || isBeforeMinimumDate
      }, getLanguageDigits(item)));
    });
  };

  return _react["default"].createElement("div", {
    className: "Calendar__yearSelectorAnimationWrapper"
  }, _react["default"].createElement("div", {
    ref: wrapperElement,
    className: "Calendar__yearSelectorWrapper"
  }, _react["default"].createElement("div", {
    ref: yearListElement,
    className: "Calendar__yearSelector",
    "data-testid": "year-selector"
  }, renderSelectorYears())));
};

YearSelector.propTypes = {
  selectorStartingYear: _propTypes["default"].number,
  selectorEndingYear: _propTypes["default"].number
};
YearSelector.defaultProps = {
  selectorStartingYear: 0,
  selectorEndingYear: 0
};
var _default = YearSelector;
exports["default"] = _default;