"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TYPE_MUTLI_DATE = exports.TYPE_RANGE = exports.TYPE_SINGLE_DATE = exports.MAXIMUM_SELECTABLE_YEAR_SUM = exports.MINIMUM_SELECTABLE_YEAR_SUBTRACT = exports.DAY_SHAPE = exports.GREGORIAN_WEEK_DAYS = exports.PERSIAN_WEEK_DAYS = exports.GREGORIAN_MONTHS = exports.PERSIAN_MONTHS = exports.PERSIAN_NUMBERS = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var PERSIAN_NUMBERS = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
exports.PERSIAN_NUMBERS = PERSIAN_NUMBERS;
var PERSIAN_MONTHS = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
exports.PERSIAN_MONTHS = PERSIAN_MONTHS;
var GREGORIAN_MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
exports.GREGORIAN_MONTHS = GREGORIAN_MONTHS;
var PERSIAN_WEEK_DAYS = ['شنبه', 'یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه'];
exports.PERSIAN_WEEK_DAYS = PERSIAN_WEEK_DAYS;
var GREGORIAN_WEEK_DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
exports.GREGORIAN_WEEK_DAYS = GREGORIAN_WEEK_DAYS;
var DAY_SHAPE = {
  year: _propTypes["default"].number.isRequired,
  month: _propTypes["default"].number.isRequired,
  day: _propTypes["default"].number.isRequired
};
exports.DAY_SHAPE = DAY_SHAPE;
var MINIMUM_SELECTABLE_YEAR_SUBTRACT = 100;
exports.MINIMUM_SELECTABLE_YEAR_SUBTRACT = MINIMUM_SELECTABLE_YEAR_SUBTRACT;
var MAXIMUM_SELECTABLE_YEAR_SUM = 50;
exports.MAXIMUM_SELECTABLE_YEAR_SUM = MAXIMUM_SELECTABLE_YEAR_SUM;
var TYPE_SINGLE_DATE = 'SINGLE_DATE';
exports.TYPE_SINGLE_DATE = TYPE_SINGLE_DATE;
var TYPE_RANGE = 'RANGE';
exports.TYPE_RANGE = TYPE_RANGE;
var TYPE_MUTLI_DATE = 'MUTLI_DATE';
exports.TYPE_MUTLI_DATE = TYPE_MUTLI_DATE;