"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.handleSlideAnimationEnd = exports.getSlideDate = exports.animateContent = void 0;

var _generalUtils = require("./generalUtils");

var getSlideDate = function getSlideDate(_ref) {
  var parent = _ref.parent,
      isInitialActiveChild = _ref.isInitialActiveChild,
      activeDate = _ref.activeDate,
      monthChangeDirection = _ref.monthChangeDirection;
  if (!parent) return activeDate;
  var child = parent.children[isInitialActiveChild ? 0 : 1];
  var isActiveSlide = child.classList.contains('-shown') || child.classList.contains('-shownAnimated'); // check -shownAnimated for Safari bug

  return isActiveSlide ? activeDate : (0, _generalUtils.getDateAccordingToMonth)(activeDate, monthChangeDirection);
};

exports.getSlideDate = getSlideDate;

var animateContent = function animateContent(_ref2) {
  var parent = _ref2.parent,
      direction = _ref2.direction;
  var wrapperChildren = Array.from(parent.children);
  var shownItem = wrapperChildren.find(function (child) {
    return child.classList.contains('-shown');
  });
  var hiddenItem = wrapperChildren.find(function (child) {
    return child !== shownItem;
  });
  var baseClass = shownItem.classList[0];
  var isNextMonth = direction === 'NEXT';

  var getAnimationClass = function getAnimationClass(value) {
    return value ? '-hiddenNext' : '-hiddenPrevious';
  };

  hiddenItem.style.transition = 'none';
  shownItem.style.transition = '';
  shownItem.className = "".concat(baseClass, " ").concat(getAnimationClass(!isNextMonth));
  hiddenItem.className = "".concat(baseClass, " ").concat(getAnimationClass(isNextMonth));
  hiddenItem.classList.add('-shownAnimated');
};

exports.animateContent = animateContent;

var handleSlideAnimationEnd = function handleSlideAnimationEnd(_ref3) {
  var target = _ref3.target;
  target.classList.remove('-hiddenNext');
  target.classList.remove('-hiddenPrevious');
  target.classList.replace('-shownAnimated', '-shown');
};

exports.handleSlideAnimationEnd = handleSlideAnimationEnd;