"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getValueType = exports.getDateAccordingToMonth = exports.deepCloneObject = exports.shallowClone = exports.toExtendedDay = exports.putZero = exports.isSameDay = exports.createUniqueRange = void 0;

var _constants = require("./constants");

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
  These utility functions don't depend on locale of the date picker(Persian or Gregorian)
*/
var createUniqueRange = function createUniqueRange(number, startingId) {
  return Array.from(Array(number).keys()).map(function (key) {
    return {
      value: key + 1,
      id: "".concat(startingId, "-").concat(key)
    };
  });
};

exports.createUniqueRange = createUniqueRange;

var isSameDay = function isSameDay(day1, day2) {
  if (!day1 || !day2) return false;
  return day1.day === day2.day && day1.month === day2.month && day1.year === day2.year;
};

exports.isSameDay = isSameDay;

var putZero = function putZero(number) {
  return number.toString().length === 1 ? "0".concat(number) : number;
};

exports.putZero = putZero;

var toExtendedDay = function toExtendedDay(date) {
  return [date.year, date.month, date.day];
};

exports.toExtendedDay = toExtendedDay;

var shallowClone = function shallowClone(value) {
  return _objectSpread({}, value);
};

exports.shallowClone = shallowClone;

var deepCloneObject = function deepCloneObject(obj) {
  return JSON.parse(JSON.stringify(obj));
};

exports.deepCloneObject = deepCloneObject;

var getDateAccordingToMonth = function getDateAccordingToMonth(date, direction) {
  var toSum = direction === 'NEXT' ? 1 : -1;
  var newMonthIndex = date.month + toSum;
  var newYear = date.year;

  if (newMonthIndex < 1) {
    newMonthIndex = 12;
    newYear -= 1;
  }

  if (newMonthIndex > 12) {
    newMonthIndex = 1;
    newYear += 1;
  }

  var newDate = {
    year: newYear,
    month: newMonthIndex,
    day: 1
  };
  return newDate;
};

exports.getDateAccordingToMonth = getDateAccordingToMonth;

var hasProperty = function hasProperty(object, propertyName) {
  return Object.prototype.hasOwnProperty.call(object || {}, propertyName);
};

var getValueType = function getValueType(value) {
  if (Array.isArray(value)) return _constants.TYPE_MUTLI_DATE;
  if (hasProperty(value, 'from') && hasProperty(value, 'to')) return _constants.TYPE_RANGE;

  if (!value || hasProperty(value, 'year') && hasProperty(value, 'month') && hasProperty(value, 'day')) {
    return _constants.TYPE_SINGLE_DATE;
  }

  throw new TypeError("The passed value is malformed! Please make sure you're using one of the valid value types for date picker.");
};

exports.getValueType = getValueType;