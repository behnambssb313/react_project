"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _jalaaliJs = _interopRequireDefault(require("jalaali-js"));

var _constants = require("./constants");

var _generalUtils = require("./generalUtils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var utils = function utils(isPersian) {
  var isGregorian = !isPersian;
  var monthsList = isGregorian ? _constants.GREGORIAN_MONTHS : _constants.PERSIAN_MONTHS;
  var weekDaysList = isGregorian ? _constants.GREGORIAN_WEEK_DAYS : _constants.PERSIAN_WEEK_DAYS;

  var getToday = function getToday() {
    var todayDate = new Date();
    var year = todayDate.getFullYear();
    var month = todayDate.getMonth() + 1;
    var day = todayDate.getDate();
    if (isGregorian) return {
      year: year,
      month: month,
      day: day
    };

    var _jalaali$toJalaali = _jalaaliJs["default"].toJalaali(year, month, day),
        jy = _jalaali$toJalaali.jy,
        jm = _jalaali$toJalaali.jm,
        jd = _jalaali$toJalaali.jd;

    var currentDate = {
      year: jy,
      month: jm,
      day: jd
    };
    return currentDate;
  };

  var getMonthName = function getMonthName(month) {
    return monthsList[month - 1];
  };

  var getMonthNumber = function getMonthNumber(monthName) {
    return monthsList.indexOf(monthName) + 1;
  };

  var toNativeDate = function toNativeDate(date) {
    if (isGregorian) return new Date(date.year, date.month - 1, date.day);

    var gregorian = _jalaaliJs["default"].toGregorian.apply(_jalaaliJs["default"], _toConsumableArray((0, _generalUtils.toExtendedDay)(date)));

    return new Date(gregorian.gy, gregorian.gm - 1, gregorian.gd);
  };

  var getMonthLength = function getMonthLength(date) {
    return isGregorian ? new Date(date.year, date.month, 0).getDate() : _jalaaliJs["default"].jalaaliMonthLength(date.year, date.month);
  };

  var getMonthFirstWeekday = function getMonthFirstWeekday(date) {
    var gregorianFirstDay = isGregorian ? {
      gy: date.year,
      gm: date.month,
      gd: 1
    } : _jalaaliJs["default"].toGregorian(date.year, date.month, 1);
    var gregorianDate = new Date(gregorianFirstDay.gy, gregorianFirstDay.gm - 1, gregorianFirstDay.gd);
    var weekday = gregorianDate.getDay();
    if (isGregorian) return weekday;
    return weekday < 6 ? weekday + 1 : 0;
  };

  var isBeforeDate = function isBeforeDate(day1, day2) {
    if (!day1 || !day2) return false;
    return toNativeDate(day1) < toNativeDate(day2);
  };

  var checkDayInDayRange = function checkDayInDayRange(_ref) {
    var day = _ref.day,
        from = _ref.from,
        to = _ref.to;
    if (!day || !from || !to) return false;
    var nativeDay = toNativeDate(day);
    var nativeFrom = toNativeDate(from);
    var nativeTo = toNativeDate(to);
    return nativeDay > nativeFrom && nativeDay < nativeTo;
  };

  var getLanguageDigits = function getLanguageDigits(digit) {
    if (isGregorian) return digit;
    return digit.toString().split('').map(function (letter) {
      return _constants.PERSIAN_NUMBERS[Number(letter)];
    }).join('');
  };

  return {
    getToday: getToday,
    getMonthName: getMonthName,
    getMonthNumber: getMonthNumber,
    getMonthLength: getMonthLength,
    getMonthFirstWeekday: getMonthFirstWeekday,
    isBeforeDate: isBeforeDate,
    checkDayInDayRange: checkDayInDayRange,
    monthsList: monthsList,
    weekDaysList: weekDaysList,
    getLanguageDigits: getLanguageDigits
  };
};

var _default = utils;
exports["default"] = _default;