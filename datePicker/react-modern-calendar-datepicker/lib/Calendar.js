"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Calendar = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _localeUtils = _interopRequireDefault(require("./shared/localeUtils"));

var _generalUtils = require("./shared/generalUtils");

var _constants = require("./shared/constants");

var _components = require("./components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Calendar = function Calendar(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      onDisabledDayError = _ref.onDisabledDayError,
      calendarClassName = _ref.calendarClassName,
      calendarTodayClassName = _ref.calendarTodayClassName,
      calendarSelectedDayClassName = _ref.calendarSelectedDayClassName,
      calendarRangeStartClassName = _ref.calendarRangeStartClassName,
      calendarRangeBetweenClassName = _ref.calendarRangeBetweenClassName,
      calendarRangeEndClassName = _ref.calendarRangeEndClassName,
      disabledDays = _ref.disabledDays,
      colorPrimary = _ref.colorPrimary,
      colorPrimaryLight = _ref.colorPrimaryLight,
      minimumDate = _ref.minimumDate,
      maximumDate = _ref.maximumDate,
      selectorStartingYear = _ref.selectorStartingYear,
      selectorEndingYear = _ref.selectorEndingYear,
      isPersian = _ref.isPersian,
      shouldHighlightWeekends = _ref.shouldHighlightWeekends;
  var calendarElement = (0, _react.useRef)(null);

  var _useState = (0, _react.useState)({
    activeDate: null,
    monthChangeDirection: '',
    isMonthSelectorOpen: false,
    isYearSelectorOpen: false
  }),
      _useState2 = _slicedToArray(_useState, 2),
      mainState = _useState2[0],
      setMainState = _useState2[1];

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getToday = _useMemo.getToday,
      weekDaysList = _useMemo.weekDaysList;

  var today = getToday();

  var createStateToggler = function createStateToggler(property) {
    return function () {
      setMainState(_objectSpread({}, mainState, _defineProperty({}, property, !mainState[property])));
    };
  };

  var toggleMonthSelector = createStateToggler('isMonthSelectorOpen');
  var toggleYearSelector = createStateToggler('isYearSelectorOpen');

  var getComputedActiveDate = function getComputedActiveDate() {
    var valueType = (0, _generalUtils.getValueType)(value);
    if (valueType === _constants.TYPE_MUTLI_DATE && value.length) return (0, _generalUtils.shallowClone)(value[0]);
    if (valueType === _constants.TYPE_SINGLE_DATE && value) return (0, _generalUtils.shallowClone)(value);
    if (valueType === _constants.TYPE_RANGE && value.from) return (0, _generalUtils.shallowClone)(value.from);
    return (0, _generalUtils.shallowClone)(today);
  };

  var activeDate = mainState.activeDate ? (0, _generalUtils.shallowClone)(mainState.activeDate) : getComputedActiveDate();

  var renderWeekDays = function renderWeekDays() {
    return weekDaysList.map(function (weekDay) {
      return _react["default"].createElement("span", {
        key: weekDay,
        className: "Calendar__weekDay"
      }, weekDay[0]);
    });
  };

  var handleMonthChange = function handleMonthChange(direction) {
    setMainState(_objectSpread({}, mainState, {
      monthChangeDirection: direction
    }));
  };

  var updateDate = function updateDate() {
    setMainState({
      activeDate: (0, _generalUtils.getDateAccordingToMonth)(activeDate, mainState.monthChangeDirection),
      monthChangeDirection: ''
    });
  };

  var selectMonth = function selectMonth(newMonthNumber) {
    setMainState(_objectSpread({}, mainState, {
      activeDate: _objectSpread({}, activeDate, {
        month: newMonthNumber
      }),
      isMonthSelectorOpen: false
    }));
  };

  var selectYear = function selectYear(year) {
    setMainState(_objectSpread({}, mainState, {
      activeDate: _objectSpread({}, activeDate, {
        year: year
      }),
      isYearSelectorOpen: false
    }));
  };

  return _react["default"].createElement("div", {
    className: "Calendar ".concat(calendarClassName, " ").concat(isPersian ? '-persian' : ''),
    style: {
      '--cl-color-primary': colorPrimary,
      '--cl-color-primary-light': colorPrimaryLight
    },
    ref: calendarElement
  }, _react["default"].createElement(_components.Header, {
    maximumDate: maximumDate,
    minimumDate: minimumDate,
    activeDate: activeDate,
    onMonthChange: handleMonthChange,
    onMonthSelect: toggleMonthSelector,
    onYearSelect: toggleYearSelector,
    monthChangeDirection: mainState.monthChangeDirection,
    isMonthSelectorOpen: mainState.isMonthSelectorOpen,
    isYearSelectorOpen: mainState.isYearSelectorOpen,
    isPersian: isPersian
  }), _react["default"].createElement(_components.MonthSelector, {
    isOpen: mainState.isMonthSelectorOpen,
    activeDate: activeDate,
    onMonthSelect: selectMonth,
    maximumDate: maximumDate,
    minimumDate: minimumDate,
    isPersian: isPersian
  }), _react["default"].createElement(_components.YearSelector, {
    isOpen: mainState.isYearSelectorOpen,
    activeDate: activeDate,
    onYearSelect: selectYear,
    selectorStartingYear: selectorStartingYear,
    selectorEndingYear: selectorEndingYear,
    maximumDate: maximumDate,
    minimumDate: minimumDate,
    isPersian: isPersian
  }), _react["default"].createElement("div", {
    className: "Calendar__weekDays"
  }, renderWeekDays()), _react["default"].createElement(_components.DaysList, {
    activeDate: activeDate,
    value: value,
    monthChangeDirection: mainState.monthChangeDirection,
    onSlideChange: updateDate,
    disabledDays: disabledDays,
    onDisabledDayError: onDisabledDayError,
    minimumDate: minimumDate,
    maximumDate: maximumDate,
    onChange: onChange,
    calendarTodayClassName: calendarTodayClassName,
    calendarSelectedDayClassName: calendarSelectedDayClassName,
    calendarRangeStartClassName: calendarRangeStartClassName,
    calendarRangeEndClassName: calendarRangeEndClassName,
    calendarRangeBetweenClassName: calendarRangeBetweenClassName,
    isPersian: isPersian,
    shouldHighlightWeekends: shouldHighlightWeekends
  }));
};

exports.Calendar = Calendar;
Calendar.defaultProps = {
  minimumDate: null,
  maximumDate: null,
  colorPrimary: '#0eca2d',
  colorPrimaryLight: '#cff4d5',
  calendarClassName: '',
  isPersian: false,
  value: null
};
Calendar.propTypes = {
  value: _propTypes["default"].oneOfType([_propTypes["default"].shape(_constants.DAY_SHAPE), _propTypes["default"].shape({
    from: _propTypes["default"].shape(_constants.DAY_SHAPE),
    to: _propTypes["default"].shape(_constants.DAY_SHAPE)
  }), _propTypes["default"].arrayOf(_propTypes["default"].shape(_constants.DAY_SHAPE))]),
  calendarClassName: _propTypes["default"].string,
  colorPrimary: _propTypes["default"].string,
  colorPrimaryLight: _propTypes["default"].string,
  minimumDate: _propTypes["default"].shape(_constants.DAY_SHAPE),
  maximumDate: _propTypes["default"].shape(_constants.DAY_SHAPE),
  isPersian: _propTypes["default"].bool
};