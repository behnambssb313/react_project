"use strict";
import { datePickerId } from "../../../src/Component/Utilities/inputGenerator";
datePickerId();
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _localeUtils = _interopRequireDefault(require("./shared/localeUtils"));

var _generalUtils = require("./shared/generalUtils");

var _constants = require("./shared/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

var DatePickerInput = _react["default"].forwardRef(function (_ref, ref) {
  var onFocus = _ref.onFocus,
      onBlur = _ref.onBlur,
      value = _ref.value,
      inputPlaceholder = _ref.inputPlaceholder,
      inputClassName = _ref.inputClassName,
      formatInputText = _ref.formatInputText,
      renderInput = _ref.renderInput,
      isPersian = _ref.isPersian;

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getLanguageDigits = _useMemo.getLanguageDigits;

  var getSingleDayValue = function getSingleDayValue() {
    if (!value) return '';
    var year = getLanguageDigits(value.year);
    var month = getLanguageDigits((0, _generalUtils.putZero)(value.month));
    var day = getLanguageDigits((0, _generalUtils.putZero)(value.day));
    return "".concat(year, "/").concat(month, "/").concat(day);
  };

  var fromWord = isPersian ? 'از' : 'from';
  var toWord = isPersian ? 'تا' : 'to';
  var yearLetterSkip = isPersian ? -2 : 0;

  var getDayRangeValue = function getDayRangeValue() {
    if (!value.from || !value.to) return '';
    var from = value.from,
        to = value.to;
    var fromText = "".concat(getLanguageDigits((0, _generalUtils.putZero)(from.year)).toString().slice(yearLetterSkip), "/").concat(getLanguageDigits((0, _generalUtils.putZero)(from.month)), "/").concat(getLanguageDigits((0, _generalUtils.putZero)(from.day)));
    var toText = "".concat(getLanguageDigits((0, _generalUtils.putZero)(to.year)).toString().slice(yearLetterSkip), "/").concat(getLanguageDigits((0, _generalUtils.putZero)(to.month)), "/").concat(getLanguageDigits((0, _generalUtils.putZero)(to.day)));
    return "".concat(fromWord, " ").concat(fromText, " ").concat(toWord, " ").concat(toText);
  };

  var getMultiDateValue = function getMultiDateValue() {
    return value.map(function (date) {
      return getLanguageDigits(date.day);
    }).join("".concat(isPersian ? '،' : ',', " "));
  };

  var getValue = function getValue() {
    if (formatInputText()) return formatInputText();
    var valueType = (0, _generalUtils.getValueType)(value);

    switch (valueType) {
      case _constants.TYPE_SINGLE_DATE:
        return getSingleDayValue();

      case _constants.TYPE_RANGE:
        return getDayRangeValue();

      case _constants.TYPE_MUTLI_DATE:
        return getMultiDateValue();
    }
  };

  var placeholderValue = inputPlaceholder || (isPersian ? 'انتخاب...' : 'Select...');

  var render = function render() {
    return renderInput({
      ref: ref,
      onFocus: onFocus,
      onBlur: onBlur
    }) || _react["default"].createElement("input", {
      "id":`}_datePicker`,
      "data-testid": "datepicker-input",
      readOnly: true,
      ref: ref,
      onFocus: onFocus,
      onBlur: onBlur,
      value: getValue(),
      placeholder: placeholderValue,
      className: "DatePicker__input".concat(isPersian ? ' -persian' : '', " ").concat(inputClassName),
      "aria-label": placeholderValue
    });
  };

  return render();
});

DatePickerInput.defaultProps = {
  formatInputText: function formatInputText() {
    return '';
  },
  renderInput: function renderInput() {
    return null;
  },
  inputPlaceholder: '',
  inputClassName: '',
  isPersian: false
};
DatePickerInput.propTypes = {
  formatInputText: _propTypes["default"].func,
  inputPlaceholder: _propTypes["default"].string,
  inputClassName: _propTypes["default"].string,
  renderInput: _propTypes["default"].func,
  isPersian: _propTypes["default"].bool
};
var _default = DatePickerInput;
exports["default"] = _default;