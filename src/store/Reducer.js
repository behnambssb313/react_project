    export const initState={
        data:[],
        current:[],

        bordered: false,
        expandedRowRender:undefined,
        size: 'small',
        rowSelection:undefined,
        
        input:[],
       
        sett:'Dashbord',

        columns:[],
        searchText:'',
        editingKey:'',
        drawerCreateData:[],
        display:'none',
        visible:false,
    }

    export const reducer=(state=initState,action)=>{
        let result=state;
        let val=''
        switch (action.type) {
                case 'DATA':
                        result={...state,data:action.payload};
                        break;
                        case 'CURRENT':
                        result={...state,current:action.payload};
                        break;

                case 'BORDERED':
                        result={...state,bordered:action.payload}
                        break;
                case 'EXPANDED_ROW_RENDER':
                        result={...state,expandedRowRender:action.payload}
                        break;
                case 'SIZE':
                        result={...state,size:action.payload}
                        break;
                case 'ROW_SELECTION':
                        result={...state,rowSelection:action.payload}
                        break;
            
                case 'SET_COLUMNS':
                        result={...state,columns:action.payload}
                        break;
                case 'SETSEARCHTEXT':
                        result={...state,searchText:action.payload}
                        break;
                case 'SETEDITINGKEY':
                        result={...state,editingKey:action.payload}
                        break;
                case 'DRAWER_CREATE_DATA':
                        console.log('number')
                        result={...state,drawerCreateData:action.payload}
                        break;
                case 'SET_INPUT':
                        console.log('behnam new ',action.value)
                        result={...state,input:{...state.input,[action.name]:action.value}};
                        break;
                case 'SET_CURRENT_RECORD':
                
                                result={...state,input:action.payload};
                                break;
                                case 'SETT':
                
                                        result={...state,sett:action.payload};
                                        break;
             

                default:
                        break;
        }
        return result;
    }

