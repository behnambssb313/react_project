import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

import { ConfigProvider   } from 'antd';
import fa_IR from 'antd/es/locale-provider/fa_IR';
import moment from "moment";
import 'moment/locale/fa'
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, NavLink, Switch } from 'react-router-dom'

import Store from "./store/Store";
import { Provider } from "react-redux";

moment.locale('fa');

ReactDOM.render(
    <Provider store={Store}>
        <ConfigProvider locale={fa_IR}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
    </ConfigProvider >
    </Provider>
, document.getElementById('root'));

serviceWorker.unregister();
    