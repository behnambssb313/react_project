import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import { Menu, Icon,Row,Col,Layout,Button,Collapse,Input,Card} from 'antd';
import { BrowserRouter ,Route,Link,Switch} from 'react-router-dom';
import axios from 'axios';
import HeaderMenu from "./Header/HeaderMenu";
import SidebarMenu from './Sidebar/SidebarMenu';


import allRoutes from './Routes'


// import { columns } from "./Contetn-Main/Staff/ColumnTable/column1";

const { SubMenu } = Menu;
const { Header, Footer, Sider, Content } = Layout;

const {Panel}=Collapse;
const {Search}=Input;

class Home extends Component{
    constructor(props){
        super(props);
        this.state = {
            articles: [],
            display:'none',
            collapsed:false,
            handel:'',
            isauth:this.props.auth
        }
    }
    componentDidMount(){
      let apiToken=localStorage.getItem('api_token');
        axios.get(`http://roocket.org/api/user?api_token=${apiToken}`)
        .then(res=>{this.setState({handel:res.data.name})
        })
        .catch(err=>{})      
    }


    componentWillUnmount(){
      let apiToken=localStorage.getItem('api_token');
      if(apiToken==null){
        console.log('sdfsd')
      }
    }

    toggle = () => {
      if(this.state.display=='none'){
        return this.setState({display:'block' })  
      }
      return this.setState({display:'none' })  
      
    };
    
    toggleCollapsed=()=>{
      this.setState({
        collapsed: !this.state.collapsed,
      });
    }
      
     
     
      
      render() {
        return (
          <div style={{height:'100%'}}>
            <Layout style={{height:'100%'}}>
              <Sider className="scroll scroll2" style={{boxShadow:' 0 0px 15px hsl(209, 100%, 8%)'}}  width={240} trigger={null} collapsible collapsed={this.state.collapsed}>
                <div className="logo"/>
                <SidebarMenu props={this.props}/>
              </Sider>
              <Layout>
                <Header className='bef'>
                  <HeaderMenu stateCollapsed={this.state.collapsed} toggleFunc={this.toggleCollapsed}  nameUser={this.state.handel} logouting={this.props.logout}/>
                </Header>
                <Content className="scroll scroll3" style={{padding:'50px 100px'}}> 
                  <Row> 
                   
                      {allRoutes.map(route=> <Route {...route}/>)}
                    
                  </Row>            
                </Content>
              </Layout>  
            </Layout>
          </div>
        );
      }
}

export default Home;