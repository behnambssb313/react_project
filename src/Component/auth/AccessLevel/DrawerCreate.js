import React,{Component} from 'react';
import { Form,Row,Drawer  } from 'antd';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import {step1} from "./DataForCreate";
import { connect } from "react-redux";


import { POST_ROLES } from "./requestApi";
import  {
        ConnectedInputGenerator,        
        ConnectedTextArea
        } from "../../Utilities/inputGenerator";


class DrawerCreate extends Component{
    constructor(props){
        super(props);
        this.state={
            visible: false, childrenDrawer: false ,childrenDrawer2: false,selectedDay: null
        }
    }
      handleSubmitDrawer1 = e => {
        e.preventDefault();
        console.log('SET_INPUT',this.props.state.input);
        POST_ROLES('',this.props.state.input)
      };

  render() {
    return (
      <div>
        <Form layout="inline" className="login-form" >
          <Row type="flex" justify="start">
            {step1.map((item)=>{
              switch (item.name) {
                case 'input':
                  return <ConnectedInputGenerator value='' props={this.props} data={item}/>
                case 'textArea':
                  return <ConnectedTextArea value='' props={this.props} data={item}/>
                default:
                  break;
              }
            })}
          </Row>
          <button class="btn btn-success" htmlType="submit" style={{float:'left'}} onClick={this.handleSubmitDrawer1}>افزودن نقش</button>
        </Form>
        </div>
     );
  }
}
const mapStateToProps=(state)=>{
  return{
    state
  }
}

const mapDispatchToProps=(dispatch)=>{
  return{}
}

const wrappedStaff = Form.create({ name: 'Staff' })(DrawerCreate);
export default connect(mapStateToProps,mapDispatchToProps)(wrappedStaff);


