import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'عنوان نقش',
        id:'comNameCom',
        required:false
    },
    {
        name:'textArea',
        title:'توضیحات',
        id:'comCorres',
        required:false
    },
]
