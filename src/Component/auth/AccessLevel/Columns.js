import React,{Component} from 'react';
import {Icon,Button,Input,Popconfirm,Divider} from 'antd';
import Highlighter from "react-highlight-words";

import { connect } from "react-redux";
import {step1} from "./DataForCreate";
import ModalEdit from "./ModalEdit";


class Columns extends Component {
  constructor(props){
    super(props);
  }
state={
  editingKey:'',
  editable:false,
  filteredInfo: null,
  sortedInfo: null,
}  

handleChange = (pagination, filters, sorter) => {
  // console.log('Various parameters', pagination, filters, sorter);
  this.setState({
    filteredInfo: filters,
    sortedInfo: sorter,
  });
};

clearFilters = () => {
  this.setState({ filteredInfo: null });
};

clearAll = () => {
  this.setState({
    filteredInfo: null,
    sortedInfo: null,
  });
};


  // search columns


  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.props.setSearchText(selectedKeys[0])
  };

  handleReset = clearFilters => {
    clearFilters();
    this.props.setSearchText('')
  };

  // delete records
  handleDelete = key => {
    const filtering=this.props.data.filter(item => item.idCom !== key);
    this.props.dataUpdate(filtering)
  };

   /// edit
   isEditing = record => record.idCom === this.state.editingKey;

   cancel = () => {
     this.setState({editingKey:''});
     this.props.setEditingKey('');
   };
 
   save(form, key) {
     form.validateFields((error, row) => {
       console.log('behnam',this.props.state.input)
       const newData = [...this.props.data];
       const index = newData.findIndex(item => key === item.idCom);
       if (index > -1) {
         const item = newData[index];
         newData.splice(index, 1, {
           ...item,
           ...this.props.state.input,
         });
         console.log('updateData',newData)
         this.props.dataUpdate(newData);
         this.setState({editingKey:''});
         this.props.setEditingKey('');
       } else {
         newData.push(row);
         this.props.dataUpdate(newData);
         this.setState({editingKey:''});
         this.props.setEditingKey('');
       }
     });
   }
 
   edit(key) {
     this.setState({editingKey:key});
     this.props.setEditingKey(key);
   }

  componentDidMount(){
    const arrayContact=[];
    const result=arrayContact.concat(step1);
    let columns=[];
    result.map(item=>{
      columns.push({
      ...item,
      dataIndex:item.id,
      align:'center',
      editable:true,
      })
    })
      columns.push(
        {
          title: 'کاربران',
          dataIndex:'users',
          align:'center',
          render: (text, record) => {
            return <Button style={{direction:'ltr'}} className="btn btn-success" icon="user"><Divider type="vertical" /><span style={{float:'right',marginTop:'6px'}}>5</span></Button>
          },
        },
        {
          title: 'مدیریت سطح دسترسی',
          dataIndex:'accessLevel',
          align:'center',
          render: (text, record) => {
            return <Button
            className="btn btn-success"

            >
              ویرایش
            </Button>
          },
        },
        {
          title: 'فعالیت ها',
          dataIndex:'action',
          align:'center',

          render: (text, record) => {
            const editable = this.isEditing(record);
            return editable ? (
              <ModalEdit
              recording={record}
              columnsing={this.props.columns}
              propsss={this.props.props} 
              onClicking={() => this.save(this.props.props.form, record.idCom)}
              onConfirming={() => this.cancel(record.idCom)}
              />
            ) : (
              this.props.data.length >= 1 ? (
                <Popconfirm title="آیا مطمئن هستید؟" onConfirm={() => this.handleDelete(record.idCom)} >
                  <a href="#/"><Icon type="delete"/></a>
                  <Divider type="vertical" />
                  <a href="#/" disabled={this.state.editingKey !== ''} onClick={() =>this.edit(record.idCom)}><Icon type="edit"/></a>
                </Popconfirm>
              ) : null
            )
          },
        }
    )
    this.props.setColumns(columns)
  }  
  render() {
    return (
      <></>
    )
  }
}


const mapStateToProps=(state)=>{
  return{
    state,
    data:state.data,
    columns:state.columns,
    searchText:state.searchText,
    editingKey:state.editingKey,
    editingg:state.editingg,
    drawerCreateData:state.drawerCreateData

  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    setColumns:(payload)=>{
      dispatch({
        type:'SET_COLUMNS',
        payload
      })
    },
    dataUpdate:(payload)=>{
      dispatch({
        type:'DATA',
        payload
      })
    },
    setSearchText:(payload)=>{
      dispatch({
        type:'SETSEARCHTEXT',
        payload
      })
    },
    setEditingKey:(payload)=>{
      dispatch({
        type:'SETEDITINGKEY',
        payload
      })
    },
    setEditing:(payload)=>{
      dispatch({
        type:'SETEDITING',
        payload
      })
    }
    
  }
}
const DataSource=connect(mapStateToProps,mapDispatchToProps);
export default DataSource(Columns)