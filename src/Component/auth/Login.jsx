import React,{useState} from 'react'
import { Link } from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox ,Row,Col,Divider } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import logo from './logo.png';


function Login(props) {
    const [fields, setFields] = useState({email:'',password:''})
    const [error, seterror] = useState({error:''})

    const handlechange=e=>{

        let dats=fields;
        dats[e.target.name]=e.target.value;
        console.log(dats)
        setFields({...dats});
    }

    const handleSubmit = e => {
        e.preventDefault();
        let {email,password}=fields;
        console.log({email,password})

        props.form.validateFields((err, values) => {
          if (!err) {
            let data=new FormData();
            data.append('email',email);
            data.append('password',password);
           
            axios.post('http://roocket.org/api/login',data)
                .then(response=>{
                    console.log(response)
                    localStorage.setItem('api_token',response.data.data.api_token);
                    props.isAuth();
                    props.history.push('/');
                  
                })
                .catch(error=>{
                    console.log(error)
                })
          }       
        });
      };
      const { getFieldDecorator } = props.form;
      return (
        <body style={{width:'100%',height:'100%'}} >
            <Row style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',backgroundColor:'#f8f8f8'}}>
                <Col style={{backgroundColor:'#fff',boxShadow:'0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',padding:'100px'}} span={8}>
                    <Form onSubmit={(e)=>handleSubmit(e)} className="login-form">
                        <img src={logo} style={{ display:'flex',margin:'0 auto'}}/>
                        <Divider dashed>دانشگاه علوم پزشکی شهرکرد</Divider>
                        <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [
                                    {type:'email', message: 'فرمت ایمیل نامعتبر است'},
                                    { required: true, message: 'ایمیل نمی تواند خالی بماند' }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="نام کاربری"
                                name="email"
                                type="email"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item style={{marginTop:'20px'}}>
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'رمزعبور نمی تواند خالی بماند' }],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                name="password"
                                placeholder="رمز عبور"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item style={{textAlign:'right'}}>
                            {getFieldDecorator('remember', {
                                valuePropName: 'checked',
                                initialValue: true,
                            })(<Checkbox>مرا به خاطرت بسپار!</Checkbox>)}
                            
                            <Link className="login-form-forgot" style={{fontWeight:'900',float:'left'}} to="/Auth/RestPassword">فراموشی رمزعبور</Link>
                        </Form.Item>
                        <Button type="primary" block htmlType="submit" className="login-form-button">
                            ورود
                        </Button>
                        <p style={{paddingTop:'20px',textAlign:'center'}}>هنوز ثبت نام نکرده ای؟!&nbsp;&nbsp;<Link style={{fontWeight:'900'}} to="/Auth/Register">ثبت نام</Link></p>
                    </Form>
                </Col>
            </Row>
        </body>
    ); 
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Login);
export default WrappedNormalLoginForm;