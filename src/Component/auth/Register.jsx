import React,{useState} from 'react'
import { Form, Input, Button ,Row,Col,Divider } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import logo from './logo.png';


function Register(props) {
    const [fields, setFields] = useState({userName:'',email:'',password:'',confirmPassword:''})
    const [error, seterror] = useState({error:''})

    const handlechange=e=>{
        let dats=fields;
        dats[e.target.name]=e.target.value;
        console.log(dats)
        setFields({...dats});
    }
    
    const handleSubmit = e => {
        e.preventDefault();
        let {userName,email,password,confirmPassword}=fields;

        props.form.validateFields((err, values) => {
          if (!err) {
            let data=new FormData();
            data.append('password',userName);
            data.append('email',email);
            data.append('password',password);
            data.append('password',confirmPassword);
            console.log(fields);
            props.history.push('/Auth/Login');

            // axios.post('http://roocket.org/api/login',data)
            //     .then(response=>{
            //         console.log(data)
            //         props.history.push('/Auth/Login');
            //     })
            //     .catch(error=>{
            //         console.log(error)
            //     })
          }       
        });
      };

    const { getFieldDecorator } = props.form;

    const compareToFirstPassword = (rule, value, callback) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('password')) {
          callback('این رمز عبور با قبلی متفاوت است!');
        } else {
          callback();
        }
      };

      return (
        <body style={{width:'100%',height:'100%'}} >
            <Row style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',backgroundColor:'#f3f3f3'}}>
                <Col style={{backgroundColor:'#fff',boxShadow:'0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',padding:'100px'}} span={8}>
                    <Form className="login-form" onSubmit={(e)=>handleSubmit(e)}>
                        <img src={logo} style={{ display:'flex',margin:'0 auto'}}/>
                        <Divider dashed>دانشگاه علوم پزشکی شهرکرد</Divider>
                        <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [
                                    { required: true, message: 'نام کاربری نمی تواند خالی بماند' }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                placeholder="نام کاربری"
                                name="userName"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [
                                    {type:'email', message: 'فرمت ایمیل نامعتبر است'},
                                    { required: true, message: 'ایمیل نمی تواند خالی بماند' }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                placeholder="ایمیل"
                                name="email"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item >
                            {getFieldDecorator('password', {
                                rules: [{ required: true, message: 'رمزعبور نمی تواند خالی بماند' }],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                type="password"
                                name="password"
                                placeholder="رمز عبور"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('confirm', {
                                rules: [{ required: true, message: 'رمزعبور نمی تواند خالی بماند' },  {
                                    validator: compareToFirstPassword,
                                  }],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                type="password"
                                name="confirmPassword"
                                placeholder="تکرار رمز عبور"
                                />,
                            )}
                        </Form.Item>
                        <Button type="primary" block htmlType="submit" className="login-form-button">
                            ثبت نام
                        </Button>
                    </Form>
                </Col>
            </Row>
        </body>
    ); 
}

const WrappedNormalLoginForm = Form.create({ name: 'normal_login' })(Register);
export default WrappedNormalLoginForm;