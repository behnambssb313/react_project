import React,{useState} from 'react'
import { Form, Input, Button ,Row,Col,Divider } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import logo from './logo.png';




function SetNewPassword(props) {
    const [fields, setFields] = useState({newPassword:'',confirmNewPassword:''})

    const handlechange=e=>{
        let dataSource=fields;
        dataSource[e.target.name]=e.target.value;
        setFields({...dataSource});
    }
    
    const handleSubmit = e => {
        e.preventDefault();
        let {newPassword,confirmNewPassword}=fields;

        props.form.validateFields((err, values) => {
          if (!err) {
            let data=new FormData();
            data.append('newPassword',newPassword);
            data.append('confirmNewPassword',confirmNewPassword);
            console.log(fields);
            // axios.post('http://roocket.org/api/login',data)
            //     .then(response=>{
            //         console.log(data)
            //         props.history.push('/Auth/Login');
            //     })
            //     .catch(error=>{
            //         console.log(error)
            //     })
          }       
        });
      };


      const compareToFirstPassword = (rule, value, callback) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('password')) {
          callback('این رمز عبور با قبلی متفاوت است!');
        } else {
          callback();
        }
      };

    const { getFieldDecorator } = props.form;
    return (
        <body style={{width:'100%',height:'100%'}} >
            <Row style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',backgroundColor:'#f3f3f3'}}>
                <Col style={{backgroundColor:'#fff',boxShadow:'0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',padding:'100px'}} span={8}>
                    <Form className="login-form" onSubmit={(e)=>handleSubmit(e)}>
                        <img src={logo} style={{ display:'flex',margin:'0 auto'}}/>
                        <Divider dashed>دانشگاه علوم پزشکی شهرکرد</Divider>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [
                                    { required: true, message: 'این فیلد نمیتواند خالی رها شود' }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                placeholder="رمز عبور جدید"
                                name="newPassword"
                                />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('confirmPassword', {
                                rules: [
                                    { required: true, message: 'نام کاربری نمی تواند خالی بماند' },
                                    {
                                        validator: compareToFirstPassword,
                                      }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                placeholder="تکرار رمز عبور جدید"
                                name="confirmNewPassword"
                                />,
                            )}
                        </Form.Item>
                        <Button type="primary" block htmlType="submit" className="login-form-button">
                            ثبت
                        </Button>
                    </Form>
                </Col>
            </Row>
        </body>

    )
}
const WrappedNormalLoginForm = Form.create({ name: 'set_new_password' })(SetNewPassword);
export default WrappedNormalLoginForm;