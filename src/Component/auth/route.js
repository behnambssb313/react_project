import Login from "./Login";
import Register from './Register';
import ResetPassword from "./ResetPassword";
import SetNewPassword from "./SetNewPassword";

export default authRoutes=[
    {
        path:'/Auth/Login',
        render:Login,
        exact:'true'
    },
    {
        path:'/Auth/Login',
        component:Register
    },
    {
        path:'/Auth/Login',
        component:ResetPassword
    },
    {
        path:'/Auth/Login',
        component:SetNewPassword
    }
]