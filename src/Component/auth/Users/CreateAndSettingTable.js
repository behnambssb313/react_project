import React,{Component} from 'react';
import { Drawer, Button, Col, Row, Icon } from 'antd';
import { connect } from "react-redux";

import DrawerSetting from "./DrawerSetting.js";
import DrawerCreate from './DrawerCreate';

class CreateAndSettingTable extends Component{

    state = { visible: false,visible2:false };

    showDrawer = () => {
      this.setState({
        visible: true,
      });
    };
    showDrawer2 = () => {
      this.setState({
        visible2: true,
      });
      this.props.setInputAll([])
    };
    onClose = () => {
      this.setState({
        visible: false,
      });
    };
    onClose2 = () => {
      this.setState({
        visible2: false,
      });
    };

    render(){
        return(
            <div>
              <Row type="flex" justify="start">
                  <Col span={2}>
                      <Button type="primary" onClick={this.showDrawer2} style={{width:'100%',lineHeight:'2'}}>
                        <Icon type="plus" />جدید
                      </Button>
                  </Col>
                  <Col span={2}>
                      <Button type="primary" onClick={this.showDrawer} style={{width:'100%',marginRight:'5px',lineHeight:'2'}}>
                        <Icon type="setting"  />تنظیمات
                      </Button>
                  </Col>
              </Row>
                <Drawer
                      title="تنظیمات"
                      width={720}
                      height={150}
                      onClose={this.onClose}
                      visible={this.state.visible}
                      placement={'top'}
                      style={{textAlign:'right'}}
                >
                  <DrawerSetting/>
                </Drawer>
                <Drawer
                    title="ایجاد کاربر جدید"
                    width={720}
                    height={195}
                    onClose={this.onClose2}
                    visible={this.state.visible2}
                    placement={'bottom'}
                    style={{textAlign:'right',direction:'rtl' }}
                  >
                  <DrawerCreate/>
                </Drawer>
            </div>
        );
    }
}

const mapStateToProps=(state)=>{
  return{}
}

const mapDispatchToProps=(dispatch)=>{
  return{
    setInputAll:(payload)=>{
     dispatch({
       type:'SET_INPUT_ALL',
       payload
     })
    }
   }
}

export default connect(mapStateToProps,mapDispatchToProps)(CreateAndSettingTable);