import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'upload',
        title:'عکس',
        class:'ant-upload-list-item-name',
        id:'image',
        required:false,
        render:(record)=>{return <Avatar src={record} shape="square" icon="user" />}
    },
    {
        name:'input',
        type:'text',
        title:'نام',
        id:'firstName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'lastName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام کاربری',
        id:'userName',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره موبایل',
        id:'phoneNumber',
        required:false
    },
    {
        name:'input',
        type:'email',
        title:'ایمیل',
        id:'email',
        required:false
    },
    // {
    //     name:'input',
    //     type:'password',
    //     title:'کلمه عبور',
    //     id:'comNameCom',
    //     required:false
    // },
    // {
    //     name:'input',
    //     type:'password',
    //     title:'تکرار کلمه عبور',
    //     id:'comNameCom',
    //     required:false
    // },
    // {
    //     name:'datePicker',
    //     title:'تاریخ تولد',
    //     id:'safDateOfBirth',
    //     required:false,
    //     palceholder:'انتخاب کنید...'
    // },
    
]

