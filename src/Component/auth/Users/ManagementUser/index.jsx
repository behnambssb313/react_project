import React,{Component} from 'react';
import { Table, Form,Row,Col,Button,Input,Card,Tabs,Icon ,Avatar,Descriptions,Switch  } from 'antd';
import { connect } from "react-redux";

import CreateAndSettingTable from '../CreateAndSettingTable';
import DataSource  from "../Columns.js";

import {GET_FAKE,GET_USERS} from "../requestApi";

// colums table
const {Search}=Input;
const pagination = { position: 'bottom' };

const { TabPane } = Tabs;

class ManagementUser extends Component {
  constructor(props){
    super(props);
  }

  state = {
    pagination,
    hasData: true,
    display:'none',
  };

  /// RequestApi: Get
    componentDidMount(){
     console.log(this.props.state.data)
    }

  // settings table evant
    toggle = () => {
      if(this.state.display=='none'){
        return this.setState({display:'block' })  
      }
      return this.setState({display:'none' })  
    };

  render() {
    const da=this.props.state.current;
    return (
          <div>
            <DataSource props={this.props} drawerCreateData={this.props.drawerCreateData}/>
              <Row type="flex" justify="center" style={{marginTop:'80px'}}>
                <Col span={6} style={{textAlign:'center'}}>
                  <Row>
                    <Avatar size={150} icon="user" />
                  </Row>
                  <Row style={{marginTop:'15px'}}>
                    <Button className="btn btn-success" >ویرایش اطلاعات کاربر</Button>
                  </Row>
                  <Row style={{marginTop:'5px'}}>
                    <Button class="btn btn-warning">تنظیم مجدد کلمه عبور</Button>
                  </Row>
                </Col>
                <Col span={18} style={{direction:'ltr',textAlign:'right'}}>
                  <Tabs defaultActiveKey="2" >
                    <TabPane tab={<span>مدیریت کاربر<Icon style={{marginLeft:'5px'}} type="user"/></span>} key="1">
                    {da.map(item=>{
                       return <ul style={{listStyle:'none',backgroundColor:'white',padding:'15px',direction:'rtl'}}>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>وضعیت حساب کاربری:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.isActive==1 && true} /></li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>وضعیت ایمیل:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.emailConfirmed==1 && true} /></li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>وضعیت شماره موبایل:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.phoneNumberConfirmed==1 && true} /></li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>امکان قفل شدن حساب کاربری:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.lockOutEnabled==1 && true} /></li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>امکان سنجی دو مرحله ای:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.towFactorEnabled==1 && true} /></li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>وضعیت قفل حساب کاربری:</span>&nbsp;&nbsp;<Switch className="green" defaultChecked={item.isActiveLock==1 && true} /></li>  
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>تعداد تلاش های ناموفق:</span>&nbsp;&nbsp;{item.accessFailedCount}</li>
                                <li style={{padding:'5px'}}><span style={{fontWeight:'bold'}}>زمان خروج از حالت قفل:</span>&nbsp;&nbsp;{item.lockOutEnd}</li>
                              </ul>
                     })}
                    </TabPane>
                    <TabPane tab={<span>اطلاعات شخصی<Icon style={{marginLeft:'5px'}} type="solution"/></span>} key="2">
                     {da.map(item=>{
                       return <ul style={{listStyle:'none',backgroundColor:'white',padding:'15px'}}>
                                <li><span style={{fontWeight:'bold'}}>نام:</span>&nbsp;&nbsp;{item.firstName}</li>
                                <li><span style={{fontWeight:'bold'}}>نام خانوادگی:</span>&nbsp;&nbsp;سرخوش</li>
                                <li><span style={{fontWeight:'bold'}}>ایمیل:</span>&nbsp;&nbsp;{item.email}</li>
                                <li><span style={{fontWeight:'bold'}}>شماره موبایل:</span>&nbsp;&nbsp;{item.phoneNumber}</li>
                                <li><span style={{fontWeight:'bold'}}>تاریخ تولد:</span>&nbsp;&nbsp;{item.data}</li>
                                <li><span style={{fontWeight:'bold'}}>تاریخ عضویت:</span>&nbsp;&nbsp;{item.data}</li>  
                                <li><span style={{fontWeight:'bold'}}>آخرین بازدید:</span>&nbsp;&nbsp;{item.data}</li>
                              </ul>
                     })}
                    </TabPane>
                  </Tabs>
                </Col>    
              </Row>
          </div>
    );
  }
}


const mapStateToProps=(state)=>{
  return{
    state
  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
   
  }
}

const   WrappedNormalContentMain = Form.create({ name: 'ManagementUser' })(ManagementUser);
export default connect(mapStateToProps,mapDispatchToProps)(WrappedNormalContentMain);  