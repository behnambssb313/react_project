import React,{useState} from 'react'
import { Form, Input, Button ,Row,Col,Divider } from 'antd';
import 'antd/dist/antd.css';
import axios from 'axios';
import logo from './logo.png';




function ResetPassword(props) {
    const [fields, setFields] = useState({email:''})

    const handlechange=e=>{
        let dats=fields;
        dats[e.target.name]=e.target.value;
        console.log(dats)
        setFields({...dats});
    }
    
    const handleSubmit = e => {
        e.preventDefault();
        let {email}=fields;

        props.form.validateFields((err, values) => {
          if (!err) {
            let data=new FormData();
            data.append('email',email);
            console.log(fields);
            // axios.post('http://roocket.org/api/login',data)
            //     .then(response=>{
            //         console.log(data)
            //         props.history.push('/Auth/Login');
            //     })
            //     .catch(error=>{
            //         console.log(error)
            //     })
          }       
        });
      };
      const { getFieldDecorator } = props.form;
    return (
        <body style={{width:'100%',height:'100%'}} >
            <Row style={{display:'flex',alignItems:'center',justifyContent:'center',height:'100%',backgroundColor:'#f3f3f3'}}>
                <Col style={{backgroundColor:'#fff',boxShadow:'0 8px 17px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',padding:'100px'}} span={8}>
                    <Form className="login-form" onSubmit={(e)=>handleSubmit(e)}>
                        <img src={logo} style={{ display:'flex',margin:'0 auto'}}/>
                        <Divider dashed>دانشگاه علوم پزشکی شهرکرد</Divider>
                        <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [
                                    { required: true, message: 'نام کاربری نمی تواند خالی بماند' }
                                ],
                            })(
                                <Input
                                onChange={(e)=>handlechange(e)}
                                placeholder="ایمیل"
                                name="email"
                                />,
                            )}
                        </Form.Item>
                        <Button type="primary" block htmlType="submit" className="login-form-button">
                            ارسال
                        </Button>
                    </Form>
                </Col>
            </Row>
        </body>

    )
}
const WrappedNormalLoginForm = Form.create({ name: 'rest_password' })(ResetPassword);
export default WrappedNormalLoginForm;