import React from 'react'
import {Route,Link} from 'react-router-dom';
import Login from "./Login";
import Register from './Register';
import ResetPassword from "./ResetPassword";
import SetNewPassword from "./SetNewPassword";
export default function auth({props,auth,isAuth}) {
    return (
        <div style={{width:'100%',height:'100%'}}>
            <Route path="/Auth/Login" exact={true} render={(props) => <Login {...props} auth={auth} isAuth={isAuth} />}/>
            <Route path="/Auth/Register" component={Register}/>
            <Route path="/Auth/RestPassword" component={ResetPassword}/>
            <Route path="/Auth/SetNewPassword" component={SetNewPassword}/>  
        </div>
    )
}
