import React from 'react';
import { Chart, Geom, Axis, Tooltip, Coord, Legend,Label} from 'bizcharts';
import { connect } from "react-redux";

class Piechart extends React.Component {
  render() {


    

    let data = [
      { item: '         مرد', count: 90, percent:0.24 },
      { item: '         زن', count: 20, percent: 0.32 },

    ];

    const sss=()=>{
       console.log('this.props.state.data',this.props.state.data)
        data.push(
           

        )
    }

    const cols = {
      percent: {
        formatter: val => (val = `${val * 100}%`),
      },
    };

    return (
      <div>{sss()}
        <Chart
          data={data}
          scale={cols}
          padding="auto"
          forceFit
        >
          <Coord type="theta" radius={0.55} />
          
          <Legend position="right" itemGap={100} itemWidth={100}  offsetY={-window.innerHeight / 2 + 120} offsetX={-100} />
          <Tooltip
            showTitle={false}
            itemTpl='<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
          />
          <Geom
            type="intervalStack"
            position="percent"
            color={['item', ['#1890ff', '#f04864']]}
            tooltip={[
              'item*percent',
              (item, percent) => {
                percent = `${percent * 100}%`;
                return {
                  name: item,
                  value: percent,
                };
              },
            ]}
            style={{
              lineWidth: 1,
              stroke: '#fff',
            }}
          >
              <Label
          content="percent"
          offset={-40}
          textStyle={{
            rotate: 0,
            textAlign: "center",
            shadowBlur: 2,
            shadowColor: "rgba(0, 0, 0, .45)"
          }}
            />
          </Geom>

        </Chart>
      </div>
    );
  }
}
const mapStateToProps=(state)=>{
    return{
      state
    }
  }
  const mapDispatchToProps=(dispatch)=>{
    return{
      bordered_FN:(payload)=>{
        dispatch({
          type:'BORDERED',
          payload
        })
      },
      // expandedRowRender_FN:(payload)=>{
      //   dispatch({
      //     type:'EXPANDED_ROW_RENDER',
      //     payload
      //   })
      // },
      size_FN:(payload)=>{
        dispatch({
          type:'SIZE',
          payload
        })
      },
      // rowSelection_FN:(payload)=>{
      //   dispatch({
      //     type:'ROW_SELECTION',
      //     payload
      //   })
      // } 
    }
  }
export default connect(mapStateToProps,mapDispatchToProps)(Piechart)
