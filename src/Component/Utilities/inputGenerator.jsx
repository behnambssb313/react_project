import React,{Component,useState,useEffect} from 'react';
import { Form, Icon, Input, Button, Checkbox ,Row,Col,Divider,Drawer,Radio,Select,AutoComplete,Upload,message} from 'antd';
import { connect } from "react-redux";

import "react-modern-calendar-datepicker/lib/DatePicker.css";
import DatePicker from "react-modern-calendar-datepicker";
import {persianToEnglish,engilshToPersian} from "./lang";



function InputGenerator({data,props,value,setInput,state}){

  const [statee, setstate] = useState(value)
  const [name, setname] = useState()

  const handleChange=(event)=>{
    setstate(event.target.value)
    setname(event.target.name)    
  }

  const vvvv=(e)=>{
    setInput(name,statee)
  }

    const { getFieldDecorator } = props.form;
    return(
        <Col span={5}>
          <Form.Item label={data.title}>
            {getFieldDecorator(data.id ? data.id : data.dataIndex, {
              rules: [{required:data.required, message:`فیلد ${data.title} نمیتواند خالی رها شود` }],
              initialValue:statee
            })(
              <Input type={data.type ? data.type : ''} name={data.id} onChange={(event)=>handleChange(event)} onBlur={(e)=>vvvv(e)}/>,
            )}
          </Form.Item>
        </Col>
    )
}



function DatePickerGenerator({props,data,value,setInput}) {
  let defaultValue=null;

  if(value!==''){
    let convert=persianToEnglish(value);
    
    let year=parseInt(convert.slice(0,4));
    let month=parseInt(convert.slice(5,6));
    let day=parseInt(convert.slice(7,9));
   defaultValue = {
    year,
    month,
    day,
  };
}

const handleChange=(e)=>{
  let name=data.id

  let year=engilshToPersian(e.year.toString())
  let month=engilshToPersian(e.month.toString());
  let day=engilshToPersian(e.day.toString());
  
  let value=`${year}/${month}/${day}`.toString()
  setInput(name,value)
}
  
  const { getFieldDecorator } = props.form;
    return(
          <Col span={5}>
            <Form.Item label={data.title}>
            {
              getFieldDecorator(data.id, {
                rules: [{required:data.required, message:`فیلد ${data.title} نمیتواند خالی رها شود` }],
                initialValue:defaultValue
              })(
                 <DatePicker style={{color:'red'}} value={defaultValue} inputPlaceholder={data.palceholder} onChange={(e)=>{handleChange(e)}} shouldHighlightWeekends isPersian/>,
                 )
            }
            </Form.Item>
          </Col>
    )
}

function RadioGenerator({data,props,value,setInput}){


  const { getFieldDecorator } = props.form;
  let counter=0;
  const [Value, setValue] = useState(value)


  const handleChange=(e)=>{
    setValue(e.target.value)
    let name=e.target.name
    let value=e.target.value
    setInput(name,value)
  }

  
  return(
        <Col span={5}>
          <Form.Item label={data.title}>
          {
              getFieldDecorator(data.id, {
                rules: [{required:data.required, message:`فیلد ${data.title} نمیتواند خالی رها شود` }],
                initialValue:Value
              })(
          <Radio.Group name={data.id} onChange={(e)=>{handleChange(e)}} defaultValue={Value}>
            {data.value.map(item=>{
              counter++
              return(
                <Radio value={counter}>{item}</Radio>
              )
            }
            )}
  
    </Radio.Group>
              )
}
          </Form.Item>
        </Col>  
  )
}

function CheckboxGenerator({data,props,value,setInput}){


  const { getFieldDecorator } = props.form;
  let counter=0;
  const [Value, setValue] = useState(value)


  const handleChange=(e)=>{
    e.target.checked ? setInput(data.id,1) : setInput(data.id,2)
  }
  
  useEffect(() => {
    setInput(data.id,2)
  }, [])

  
  return(
        <Col span={5}>
          <Form.Item label={data.title}>
          {
              getFieldDecorator(data.id, {
                rules: [{required:data.required, message:`فیلد ${data.title} نمیتواند خالی رها شود` }],
                initialValue:Value
              })(
          <Radio.Group name={data.id} onChange={(e)=>{handleChange(e)}} defaultValue={Value}>
                <Checkbox onChange={(e)=>handleChange(e)}></Checkbox>
    </Radio.Group>
              )
}
          </Form.Item>
        </Col>  
  )
}

function SelectGenerator({props,data,value,setInput}){
  const { getFieldDecorator } = props.form;
  const { Option } = Select;

  const handleChange=(e)=>{
    setInput(data.id,e)
  }

  return(
        <Col span={5}>
          <Form.Item label={data.title} >
          {getFieldDecorator(data.id ? data.id : data.dataIndex, {
            rules: [{ required: data.required, message:`فیلد ${data.title} نمی تواند خالی رها شود`}],
            initialValue:value
          })(
            <Select style={{ width: 181 }} onChange={(e)=>{handleChange(e)}} placeholder={data.palceholder}>
              {data.value.map(item=><Option value={item}>{item}</Option>)}            
            </Select>
          )}
          </Form.Item>
        </Col>
  )
} 

function AutoComplateGenerator({props,data,value,setInput}){
  const { getFieldDecorator } = props.form;
  const { Option, OptGroup } = AutoComplete;

  const options = data.data.map(opt => (
        <Option key={opt.title} value={opt.title}>
          {opt.title}
        </Option>
  ))

  const handleChange=(e)=>{
    setInput(data.id,e)
  }

  return(
    <Col span={5}>
      <Form.Item label={data.title}>
          {getFieldDecorator(data.id ? data.id : data.dataIndex, {
            rules: [
                { required: data.required, message:`فیلد ${data.title} نمی تواند خالی رها شود`}
            ],
            initialValue:value
            })(
            <AutoComplete
              className="certain-category-search"
              dropdownClassName="certain-category-search-dropdown"
              dropdownMatchSelectWidth={false}
              style={{ width: 181,textAlign:'right' }}
              dataSource={options}
              onChange={(e)=>{handleChange(e)}}
            >
              <Input type={data.type ? data.type : ''}  suffix={<Icon type="search" className="certain-category-icon" />} />
            </AutoComplete>                
          )}
      </Form.Item>
    </Col>                           
  )
}

function UploadGenerator({data,props,value,setInput}){
  
  const { getFieldDecorator } = props.form;

  const handleChange=(e)=>{
    setInput(data.id,e.file.name)
  }

  const propss = {
    name: 'file',
    multiple:false,
    accept:'.png',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
      authorization: 'authorization-text',
    },
    onChange(info,e) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`با موفقیت آپلود شد ${info.file.name}`);
        handleChange(info)
      } else if (info.file.status === 'error') {
        message.error(`ناموفق بود ${info.file.name} آپلود فایل`);
      }
    },
  };

  const fileList = [
    {
      uid: '-1',
      name: `${value}`,
      status: 'done',
      thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    },
  
  ];
  
  const props2 = {
    multiple:false,
    accept:'.png',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
      authorization: 'authorization-text',
    },
    listType: 'picture',
    defaultFileList: [...fileList],
    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        message.success(`با موفقیت آپلود شد ${info.file.name}`);
        handleChange(info)
      } else if (info.file.status === 'error') {
        message.error(`ناموفق بود ${info.file.name} آپلود فایل`);
      }
    },
  };

  return(
    <Col span={5} style={{height:value ? 120 : 75}}>
      <Form.Item label={data.title}>
        {getFieldDecorator(data.id ? data.id : data.dataIndex, {
          rules: [
          { required: data.required, message:`فیلد ${data.title} نمی تواند خالی رها شود`}],
          
          })(
            value =='' ?
         <Upload {...propss}>
          <Button>
          اپلود تصویر
          </Button>
         </Upload> 
         :
         <Upload {...props2}>
         <Button>
         {data.dataIndex ? 'اپلود تصویر جدید' : 'اپلود تصویر'}
         </Button>
        </Upload> 
        )}
      </Form.Item>
    </Col>
  
  )
  
}

function TextArea({data,props,value,setInput}){
  const { getFieldDecorator } = props.form;
  const { TextArea } = Input;


  const [statee, setstate] = useState(value)
  const [name, setname] = useState(data.id)

  const handleChange=(e)=>{
    setstate(e.target.value)
  }
  const vvvv=(e)=>{
    setInput(name,statee)
  }
  return(
        <Col span={5}>
          <Form.Item label={data.title}>
            {getFieldDecorator(data.id ? data.id : data.dataIndex, {
              rules: [{required:data.required, message:`فیلد ${data.title} نمی تواند خالی رها شود`}],
              initialValue:value
            })(
              <TextArea onChange={(e)=>{handleChange(e)}} onBlur={(e)=>vvvv(e)}/>,
            )}
          </Form.Item>
        </Col>
  )
}


const mapStateToProps=(state)=>{
  return{
    state
  }
}

const mapDispatchToProps=(dispatch)=>{
  return{
    setInput:(name,value)=>{
      dispatch({                              
        type:`SET_INPUT`,
        name,
        value
      })
    }
  }
}
export const ConnectedInputGenerator = connect(mapStateToProps,mapDispatchToProps)(InputGenerator)
export const ConnectedDatePickerGenerator = connect(mapStateToProps,mapDispatchToProps)(DatePickerGenerator)
export const ConnectedRadioGenerator = connect(mapStateToProps,mapDispatchToProps)(RadioGenerator)
export const ConnectedSelectGenerator = connect(mapStateToProps,mapDispatchToProps)(SelectGenerator)
export const ConnectedAutoComplateGenerator = connect(mapStateToProps,mapDispatchToProps)(AutoComplateGenerator)
export const ConnectedTextArea = connect(mapStateToProps,mapDispatchToProps)(TextArea)
export const ConnectedUploadGenerator = connect(mapStateToProps,mapDispatchToProps)(UploadGenerator)
export const ConnectedCheckboxGenerator = connect(mapStateToProps,mapDispatchToProps)(CheckboxGenerator)
