const pToN={
    '۰':0,
    '۱':1,
    '۲':2,
    '۳':3,
    '۴':4,
    '۵':5,
    '۶':6,
    '۷':7,
    '۸':8,
    '۹':9,
    
}
const nToP={
    0:'۰',
    1:'۱',
    2:'۲',
    3:'۳',
    4:'۴',
    5:'۵',
    6:'۶',
    7:'۷',
    8:'۸',
    9:'۹',
    
}

export const engilshToPersian=(input)=>{
    console.log('input',input)
    return input.split('').map(number=>nToP[number] ? nToP[number] : number).join('');
}
export const persianToEnglish=(input)=>{
    console.log('input',input)
    return input.split('').map(number=>pToN[number] ? pToN[number] : number).join('');
}

