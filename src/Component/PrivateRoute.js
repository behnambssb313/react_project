import React,{Component} from 'react';
import PropTyps from 'prop-types';
import {Redirect,Route} from 'react-router-dom';

class PrivateRoute extends Component{
    render(){
        const { component : Component , auth : isAuthenticated  ,ddd,...restProps } = this.props;
        return <Route {...restProps} render={(props)=>(
            isAuthenticated ? (
                <Component {...props} logout={ddd}/>
            ) : (
                <Redirect to={{ pathname : '/Auth/Login' , state : { from : props.location} }}/>
            )
        )}/>
    }
}

export default PrivateRoute;