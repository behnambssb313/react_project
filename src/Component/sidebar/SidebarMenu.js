import React,{Component} from 'react';
import { Route,Link} from 'react-router-dom';
import { Menu, Icon, Button } from 'antd';

const { SubMenu } = Menu;

class SidebarMenu extends Component {

  constructor(props){
    super(props)
  }
  
  state = {
    collapsed: false,
    OpenKeys:this.props.props.location.pathname.split('/')[1],
    SelectedKeys:!this.props.props.location.pathname.split('/')[2] ? 'Dashbord': this.props.props.location.pathname.split('/')[2]

  };
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  render() {
    return (
      <div>
        {/* <div className="logo" style={{backgroundColor:'#001529',height:'64px'}}></div> */}
        <Menu
          defaultOpenKeys={[`${this.state.OpenKeys}`]}
          defaultSelectedKeys={[`${this.state.SelectedKeys}`]}
          mode="inline"
          theme="dark"
        >  
          <Menu.Item key="Dashbord">
            <Icon type="dashboard"/>
            <span><Link to="/" key="Dashbord" className="sidebar-menu-li-a"  style={{color:'white',marginRight:'10px'}} >پیشخوان</Link></span>
          </Menu.Item>
          <SubMenu 
            key="Secretariat"
            title={
              <span>
                <Icon type="bank"/>
                <span style={{fontSize:16,marginRight:'10px'}} >دبیرخانه</span>
              </span>
            }
          >
            <Menu.Item key="Medical-Emergency"><Link to="/Secretariat/Medical-Emergency"><span>فوریتهای پزشکی</span></Link></Menu.Item>
            <Menu.Item key="Birds-Flu"><Link to="/Secretariat/Birds-Flu"><span>آنفولانرای پرندگان</span></Link></Menu.Item>
            <Menu.Item key="Complaining-People"><Link to="/Secretariat/Complaining-People"><span>شکایت مردم</span></Link></Menu.Item>
            <Menu.Item key="Suicide"><Link to="/Secretariat/Suicide"><span>آمار خودکشی</span></Link></Menu.Item>
            <Menu.Item key="Demands-From-Insurance"><Link to="/Secretariat/Demands-From-Insurance"><span>مطالبات از بیمه</span></Link></Menu.Item>
            <Menu.Item key="Credit-Deduction"><Link to="/Secretariat/Credit-Deduction"><span>کسری اعتبارات</span></Link></Menu.Item>
            <Menu.Item key="Secretariat-Companies"><Link to="/Secretariat/Secretariat-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="Secretariat-Manuals"><Link to="/Secretariat/Secretariat-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="Secretariat-ProtectiveActionPlan"><Link to="/Secretariat/Secretariat-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="Secretariat-Visit"><Link to="/Secretariat/Secretariat-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="Documents"
            title={
              <span>
                <Icon type="file"/>
                <span style={{fontSize:16,marginRight:'10px'}} >اسناد</span>
              </span>
            }
          >
            <Menu.Item key="Document-Protection"><Link to="/Documents/Document-Protection"><span>حفاطت اسناد</span></Link></Menu.Item>
            <Menu.Item key="Dispossession-Of-Documents"><Link to="/Documents/Dispossession-Of-Documents"><span>امحاء اسناد</span></Link></Menu.Item>
            <Menu.Item key="Forging-Documents"><Link to="/Documents/Forging-Documents"><span>جعل اسناد</span></Link></Menu.Item>
            <Menu.Item key="Construction-Stamp"><Link to="/Documents/Construction-Stamp"><span>ساخت مهر</span></Link></Menu.Item>
            <Menu.Item key="Missing-Stamp"><Link to="/Documents/Missing-Stamp"><span>مهر گمشده</span></Link></Menu.Item>
            <Menu.Item key="Fake-Stamp"><Link to="/Documents/Fake-Stamp"><span>جعل مهر</span></Link></Menu.Item>
            <Menu.Item key="Documents-Companies"><Link to="/Documents/Documents-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="Documents-Manuals"><Link to="/Documents/Documents-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="Documents-ProtectiveActionPlan"><Link to="/Documents/Documents-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="Documents-Visit"><Link to="/Documents/Documents-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="Personnel"
            title={
              <span>
                <Icon type="idcard"/>
                <span style={{fontSize:16,marginRight:'10px'}} >پرسنلی</span>
              </span>
            }
          >
            <Menu.Item key="Staff"><Link to="/Personnel/Staff" ><span>کارکنان</span></Link></Menu.Item>
            <Menu.Item key="Security-Staff"><Link to="/Personnel/Security-Staff" ><span>کارکنان حراست</span></Link></Menu.Item>
            <Menu.Item key="Leaving-Addiction"><Link to="/Personnel/Leaving-Addiction" ><span>ترک اعتیاد</span></Link></Menu.Item>
            <Menu.Item key="Leaving-Addiction-Center"><Link to="/Personnel/Leaving-Addiction-Center" ><span>مراکز ترک اعتیاد</span></Link></Menu.Item>
            <Menu.Item key="Student"><Link to="/Personnel/Student" ><span>دانشجو</span></Link></Menu.Item>
            <Menu.Item key="Delegation-Of-Violations"><Link to="/Personnel/Delegation-Of-Violations" ><span>هیئت تخلفات</span></Link></Menu.Item>
            <Menu.Item key="Personnel-Companies"><Link to="/Personnel/Personnel-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="Personnel-Manuals"><Link to="/Personnel/Personnel-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="Personnel-ProtectiveActionPlan"><Link to="/Personnel/Personnel-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="Personnel-Visit"><Link to="/Personnel/Personnel-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="Physical"
            title={
              <span>
                <Icon type="hdd" />
                <span style={{fontSize:16,marginRight:'10px'}} >فیزیکی</span>
              </span>
            }
          >
            <Menu.Item key="Passive-Defense"><Link to="/Physical/Passive-Defense" ><span>پدافند غیرعامل</span></Link></Menu.Item>
            <Menu.Item key="Sports-Is-Safe"><Link to="/Physical/Sports-Is-Safe" ><span>امور ورزش حراست</span></Link></Menu.Item>  
            <Menu.Item key="Physical-Companies"><Link to="/Physical/Physical-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="Physical-Manuals"><Link to="/Physical/Physical-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="Physical-ProtectiveActionPlan"><Link to="/Physical/Physical-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="Physical-Visit"><Link to="/Physical/Physical-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="Education"
            title={
              <span>
                <Icon type="read" />
                <span style={{fontSize:16,marginRight:'10px'}} >آموزش</span>
              </span>
            }
          >
            <Menu.Item key="Training-Course"><Link to="/Education/Training-Course" ><span>دوره آموزشی</span></Link></Menu.Item>
            <Menu.Item key="Education-Companies"><Link to="/Education/Education-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="Education-Manuals"><Link to="/Education/Education-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="Education-ProtectiveActionPlan"><Link to="/Education/Education-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="Education-Visit"><Link to="/Education/Education-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="It"
            title={
              <span>
                <Icon type="global" />
                <span style={{fontSize:16,marginRight:'10px'}} >فناوری اطلاعات</span>
              </span>
            }
          >
            <Menu.Item key="Computer-And-Peripherals"><Link to="/It/Computer-And-Peripherals" ><span>کامپیوتر و تجهیزات شبکه</span></Link></Menu.Item>
            <Menu.Item key="Network-Equipment"><Link to="/It/Network-Equipment" ><span>تجیزات شبکه</span></Link></Menu.Item>
            <Menu.Item key="Software"><Link to="/It/Software" ><span>نرم افزار</span></Link></Menu.Item>
            <Menu.Item key="It-Companies"><Link to="/It/It-Companies"><span>شرکت ها</span></Link></Menu.Item>
            <Menu.Item key="It-Manuals"><Link to="/It/It-Manuals"><span>دستور العمل ها</span></Link></Menu.Item>
            <Menu.Item key="It-ProtectiveActionPlan"><Link to="/It/It-ProtectiveActionPlan"><span>برنامه عملیاتی حراست</span></Link></Menu.Item>
            <Menu.Item key="It-Visit"><Link to="/It/It-Visit"><span>بازدید</span></Link></Menu.Item>
          </SubMenu>
          <SubMenu 
            key="Settings"
            title={
              <span>
                <Icon type="setting" />
                <span style={{fontSize:16,marginRight:'10px'}} >تنظیمات</span>
              </span>
            }
          >
            <Menu.Item key="Roles"><Link to="/Settings/Roles" ><span>نقش ها</span></Link></Menu.Item>
            <Menu.Item key="Users"><Link to="/Settings/Users" ><span>کاربران</span></Link></Menu.Item>
            <Menu.Item key="Access-Level"><Link to="/Settings/Access-Level" ><span>سطوح دسترسی</span></Link></Menu.Item>
          </SubMenu>

        </Menu>
      </div>
    );
  }
}

export default SidebarMenu;
