import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[

    {
        name:'input',
        type:'text',
        title:'عنوان دوره',
        id:'tcTitle',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ برگزاری',
        id:'tcDateHeld',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'datePicker',
        title:'تاریخ آزمون',
        id:'tcExamDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'number',
        title:'تعداد ساعت',
        id:'tcNumberOfHours',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'واحد برگزار کننده',
        id:'idUnit',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'autoComplate',
        type:'text',
        title:'نوع دوره',
        id:'idCourseType',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'autoComplate',
        type:'text',
        title:'نحوه برگزاری ازمون',
        id:'idHowToHold',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'شماره مجوز',
        id:'tcLicenseNumber',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد شرکت کننده',
        id:'tcNumberOfParticipants',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'محل برگزاری',
        id:'tcEventPlace',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تعداد دوره ',
        id:'tcNumberOfCourses',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'lavVist',
        required:false,
    },
]

