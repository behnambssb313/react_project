import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)  
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                idTc: current.password,
                tcTitle:'-',
                tcDateHeld:'۱۳۹۸/۹/۲۲',
                tcExamDate:'۱۳۹۸/۹/۲۲',
                tcNumberOfHours:12,
                idUnit:'-',
                idCourseType:'-',
                idHowToHold:'-',
                tcLicenseNumber:758,
                tcNumberOfParticipants:5,
                tcEventPlace:'-',
                tcNumberOfCourses:5,
                lavVist:12,
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_TRAINING_COURSE=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_TRAINING_COURSE=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_TRAINING_COURSE=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_TRAINING_COURSE=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}