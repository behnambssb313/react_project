import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idCom',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام شرکت',
        id:'comNameCom',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ ثبت شرکت',
        id:'comData',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'نام مدیر',
        id:'comNameM',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی مدیر',
        id:'comFamilyM',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد کارکنان',
        id:'comNEmployee',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نوع شرکت',
        id:'comCompanyType',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'واحد محل فعالیت',
        id:'comLocation',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره تماس',
        id:'comPhone',
        required:false
    },
    {
        name:'textArea',
        title:'آدرس دقیق',
        id:'comAddress',
        required:false
    },


]

export const step2=[
    {
        name:'input',
        type:'number',
        title:'مبلغ قرارداد',
        id:'comContratCcost',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'محل اجرا قرار داد',
        id:'comLocation',
        required:false
    },
        {
        name:'input',
        type:'text',
        title:'نوع واگذاری',
        id:'comTOA',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ شروع',
        id:'comDateStart',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'datePicker',
        title:'تاریخ پایان',
        id:'comDateEnd',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'number',
        title:'مبلغ تضمین',
        id:'comGuaranteeAmount',
        required:false
    },
    {
        name:'textArea',
        title:'تذکرات کتبی',
        id:'comWrittenNote',
        required:false
    },
    {
        name:'textArea',
        title:'وضعیت رضایت',
        id:'comSatisfactionStatus',
        required:false
    },
    {
        name:'textArea',
        title:'ملاحضات',
        id:'comConsiderations',
        required:false
    },
    {
        name:'textArea',
        title:'مکاتبه',
        id:'comCorres',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'comVisit',
        required:false
    },
]
