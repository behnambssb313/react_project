import TrainingCourse from "./training_course";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";

const EducationRoutes=[
    {
        path:'/Education/Training-Course',
        component:TrainingCourse,
    },
    {
        path:'/Education/Companies',
        component:Companies,
    },
    {
        path:'/Education/Manuals',
        component:Manuals,
    },
    {
        path:'/Education/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/Education/Visit',
        component:Visit,
    },
];


export default EducationRoutes;
