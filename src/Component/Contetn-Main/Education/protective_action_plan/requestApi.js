import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی


export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)  
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                idPap: current.password,
                papFEIM:'-',
                papIITEOD:'-',
                papPersonnelProtectio:'-',
                papIt:'-',
                papPPI:'-',
                papPSI:'-',
                papDefects:'-',
                papCauseOfDefect:'-'
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_PROTECTIVE_ACTION_PLAN=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_PROTECTIVE_ACTION_PLAN=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_PROTECTIVE_ACTION_PLAN=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_PROTECTIVE_ACTION_PLAN=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}