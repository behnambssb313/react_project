import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idMan',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'دستورالعمل',
        id:'idInstructions',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'موضوع',
        id:'ghaName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'دستور دهنده',
        id:'idSaf',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'محل ارجاع',
        id:'idUnit',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ',
        id:'ghaDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'ghaVisit',
        required:false,
    },
]

