import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'کد اقدامات',
        id:'idActions',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره صورت جلسه',
        id:'ddDate',
        required:false
    },
    {
        name:'textArea',
        title:'مکاتبات',
        id:'ddCorrespondence',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی کارکنان',
        id:'idSaf',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'ddVist',
        required:false,
    },
]

