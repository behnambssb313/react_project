import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'عنوان مهر',
        id:'fsTitle',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام جاعل',
        id:'fsName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی جاعل',
        id:'fsFamily',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ',
        id:'fsDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'autoComplate',
        type:'text',
        title:'تحویل گیرنده',
        id:'idSaf',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'dpVist',
        required:false,
    },
]

