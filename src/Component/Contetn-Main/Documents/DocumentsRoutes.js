import DocumentProtection from "./document_protection";
import DispossessionOfDocuments from "./dispossession_of_documents";
import ForgingDocuments from "./forging_documents";
import ConstructionStamp from "./construction_stamp";
import MissingStamp from "./missing_stamp";
import FakeStamp from "./fake_stamp";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";


const DocumentsRoutes=[
    {
        path:'/Documents/Document-Protection',
        component:DocumentProtection,
    },
    {
        path:'/Documents/Dispossession-Of-Documents',
        component:DispossessionOfDocuments,
    },
    {
        path:'/Documents/Forging-Documents',
        component:ForgingDocuments,
    },
    {
        path:'/Documents/Construction-Stamp',
        component:ConstructionStamp,
    },
    {
        path:'/Documents/Missing-Stamp',
        component:MissingStamp,
    },
    {
        path:'/Documents/Fake-Stamp',
        component:FakeStamp,
    },
    {
        path:'/Documents/Companies',
        component:Companies,
    },
    {
        path:'/Documents/Manuals',
        component:Manuals,
    },
    {
        path:'/Documents/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/Documents/Visit',
        component:Visit,
    },
];


export default DocumentsRoutes;
