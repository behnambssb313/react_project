import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'عنوان مهر',
        id:'msTitle',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'تحویل گیرنده',
        id:'idSaf',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'datePicker',
        title:'تاریخ مفقودی',
        id:'fsDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'autoComplate',
        type:'text',
        title:'نام واحد',
        id:'idUnit',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'textArea',
        title:'مکاتبات',
        id:'msCorres',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'msVist',
        required:false,
    },
]

