import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'autoComplate',
        type:'text',
        title:'واحد استفاده کننده',
        id:'idUnit',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'text',
        title:'نام تحویل گیرنده',
        id:'csName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی تحویل گیرنده',
        id:'csFamily',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ تحویل',
        id:'csDeliveryDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'امضا تحویل گیرنده',
        id:'csSignatureDelivery',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نمونه مهر',
        id:'csSampleStamp',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد',
        id:'csNumberStamp',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'csVist',
        required:false,
    },
]

