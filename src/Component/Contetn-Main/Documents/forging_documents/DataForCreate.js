import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'عنوان سند',
        id:'fdTitle',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام جاعل',
        id:'fdName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'fdFamily',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی جاعل',
        id:'fdNationalCode',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ',
        id:'fdDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'محل تولید',
        id:'fdPlaceOfProduction',
        required:false
    },
    {
        name:'textArea',
        title:'اقدمات صورت گرفته',
        id:'fdActions',
        required:false,
    },
    {
        name:'textArea',
        title:'نتیجه',
        id:'fdResult',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'fdVist',
        required:false,
    },
]

