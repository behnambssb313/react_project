import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'کد اقدامات',
        id:'idActions',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ',
        id:'dpDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'textArea',
        title:'مکاتبات',
        id:'dpCorrespondence',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی کارکنان',
        id:'idSaf',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'dpVist',
        required:false,
    },
]

