import React,{Component} from 'react';
import { Statistic, Card, Row, Col, Icon ,Radio} from 'antd';


class Dashbord extends Component{
    render(){
        return(

               
          <Row gutter={20} type="flex" justify="space-around" style={{textAlign:'right'}}> 
              <Col span={6} style={{color:'#001529 !important'}}>
                <Card style={{boxShadow:'rgb(210, 210, 210) 5px 5px 10px'}}>
                <Statistic
                     title={<Icon type="team" />}
                     prefix="کل کارمندان"
                     value={95}
                     valueStyle={{ color: '#1890ff' }}
                  />
                </Card>
              </Col>
              <Col span={6} style={{color:'#001529 !important'}}>
                <Card style={{boxShadow:'rgb(210, 210, 210) 5px 5px 10px'}}>
                <Statistic
                     title={<Icon type="user" />}
                     prefix="کاربران جدید"
                     value={95}
                     valueStyle={{ color: '#1890ff' }}
                  />
                </Card>
              </Col>
              <Col span={6} style={{color:'#001529 !important'}}>
                <Card style={{boxShadow:'rgb(210, 210, 210) 5px 5px 10px'}}>
                <Statistic
                     title={<Icon type="strikethrough" />}
                     prefix="نرخ تبدیل"
                     value={95}
                     valueStyle={{ color: '#1890ff' }}
                  />
                </Card>
              </Col>
              <Col span={6} style={{color:'#001529 !important'}}>
                <Card style={{boxShadow:'rgb(210, 210, 210) 5px 5px 10px'}}>
                  <Statistic
                     title={<Icon type="container" />}
                     prefix="درخواست ها"
                     value={95}
                     valueStyle={{ color: '#1890ff' }}
                  />
                </Card>
              </Col>
          </Row>

        );
    }
}

export default Dashbord;