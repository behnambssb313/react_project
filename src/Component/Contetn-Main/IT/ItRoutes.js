import ComputerAndPeripherals from "./computer_and_peripherals";
import NetworkEquipment from "./network_equipment";
import Software from "./software";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";

const ItRoutes=[
    {
        path:'/It/ComputerAndPeripherals',
        component:ComputerAndPeripherals,
    },
    {
        path:'/It/Network-Equipment',
        component:NetworkEquipment,
    },
    {
        path:'/It/Software',
        component:Software,
    },
    {
        path:'/It/Companies',
        component:Companies,
    },
    {
        path:'/It/Manuals',
        component:Manuals,
    },
    {
        path:'/It/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/It/Visit',
        component:Visit,
    },
];


export default ItRoutes;
