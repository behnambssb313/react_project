import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[

    {
        name:'input',
        type:'number',
        title:'تعداد سوئیچ',
        id:'neNSwitch',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد پنل',
        id:'neNpanel',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد مودم خارجی',
        id:'neNExternalModem',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد مودم داخلی',
        id:'neNInternalModem',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد میکروتیک',
        id:'neNMicrotics',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد NAS',
        id:'neNNAS',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد SAN',
        id:'neNSAN',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد UPS ',
        id:'neNUPS',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد نود شبکه ',
        id:'neNNodNetwork',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'کارشناس مربوط',
        id:'idSaf',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'neVisi',
        required:false,
    },
]

