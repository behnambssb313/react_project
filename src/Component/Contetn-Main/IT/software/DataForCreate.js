import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[

    {
        name:'input',
        type:'text',
        title:'نام شرکت',
        id:'softName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'عنوان تجاری محصول',
        id:'sofBusinessTitle',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ انعقاد قرارداد',
        id:'sofDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'مورد استفاده',
        id:'softUses',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'واحد بهره برداری',
        id:'softOperationUnit',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'سوپروایزر',
        id:'sofSupervisor',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'گواهی امنیت',
        id:'idEvaluationCertificate',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'autoComplate',
        type:'text',
        title:'شماره ثبت نرم افزار',
        id:'idSoftwareRegistration',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'تاریخ ثبت اطلاعات',
        id:'softNUses',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'softVisi',
        required:false,
    },
]

