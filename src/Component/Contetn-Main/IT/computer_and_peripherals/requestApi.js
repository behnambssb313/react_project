import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)  
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                idCap: current.password,
                capHardwareID:'b12',
                capUnitName:'-',
                capNcase:26,
                capNMonitor:26,
                capNkeyboard:26,
                capNMouse:12,
                capNAllInOne:69,
                capNLapto:69,
                capNTable:8,
                capNScanne:7,
                capNexternalhard:1,
                capNFlas:3,
                idSaf:'-',
                capVisi:12,
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_COMPUTER_AND_PERIPHERALS=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_COMPUTER_AND_PERIPHERALS=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_COMPUTER_AND_PERIPHERALS=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_COMPUTER_AND_PERIPHERALS=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}