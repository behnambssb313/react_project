import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شناسه سخت افزار',
        id:'capHardwareID',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام واحد',
        id:'capUnitName',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد کیس',
        id:'capNcase',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد مانیور',
        id:'capNMonitor',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد کیبورد',
        id:'capNkeyboard',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد موس',
        id:'capNMouse',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعدادAll in one',
        id:'capNAllInOne',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد  لب تاب',
        id:'capNLapto',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد تبلت',
        id:'capNTable',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد اسکنر',
        id:'capNScanne',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد هارد اکسترنال',
        id:'capNexternalhard',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'تعداد فلش',
        id:'capNFlas',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'تحویل گیرنده ',
        id:'idSaf',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'pdVist',
        required:false,
    },
]

