import React,{Component} from 'react';
import {Switch, Radio, Form} from 'antd';
import { connect } from "react-redux";

const FormItem = Form.Item;

class DrawerSetting extends Component {

  handleToggle = enable => {
    return( enable ? this.props.bordered_FN('bordered') : this.props.bordered_FN(undefined))
  };

  // handleExpandChange = enable =>{
  //   return( enable ? this.props.expandedRowRender_FN(expandedRowRender) : this.props.expandedRowRender_FN(undefined))
  // };

  handleSizeChange = e => {
   return(this.props.size_FN(e.target.value))
  };

  // handleRowSelectionChange = enable => {
  //   return ( enable ? this.props.rowSelection_FN({}) : this.props.rowSelection_FN(undefined) )
  // };

  render() {
    return (
      <Form layout="inline">
        <Form.Item label="حاشیه">
          <Switch checked={this.props.state.bordered} onChange={this.handleToggle} />
        </Form.Item>
        {/* <FormItem label="توضیحات">
          <Switch checked={!!this.props.state.expandedRowRender} onChange={this.handleExpandChange} />
        </FormItem> */}
        <FormItem label="اندازه">
          <Radio.Group size="default" value={this.props.state.size} onChange={this.handleSizeChange}>
            <Radio.Button value="default">پیش فرض</Radio.Button>
            <Radio.Button value="middle">متوسط</Radio.Button>
            <Radio.Button value="small">کوچک</Radio.Button>
          </Radio.Group>
        </FormItem>
        {/* <FormItem label="انتخاب">
          <Switch checked={!!this.props.state.rowSelection} onChange={this.handleRowSelectionChange} />
        </FormItem> */}
      </Form>
    )
  }
}

const mapStateToProps=(state)=>{
  return{
    state
  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    bordered_FN:(payload)=>{
      dispatch({
        type:'BORDERED',
        payload
      })
    },
    // expandedRowRender_FN:(payload)=>{
    //   dispatch({
    //     type:'EXPANDED_ROW_RENDER',
    //     payload
    //   })
    // },
    size_FN:(payload)=>{
      dispatch({
        type:'SIZE',
        payload
      })
    },
    // rowSelection_FN:(payload)=>{
    //   dispatch({
    //     type:'ROW_SELECTION',
    //     payload
    //   })
    // } 
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(DrawerSetting)