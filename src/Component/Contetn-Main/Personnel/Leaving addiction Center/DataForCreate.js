import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idLac',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام مرکز',
        id:'lacNCenter',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام موسس',
        id:'lacName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی موسس',
        id:'lacfamily',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی موسس',
        id:'lacNcode',
        required:false
    },
    {
        name:'textArea',
        type:'number',
        title:'آدرس',
        id:'lacAddress',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'سال تاسیس',
        id:'lacDate',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'lacVist',
        required:false,
    },
]

