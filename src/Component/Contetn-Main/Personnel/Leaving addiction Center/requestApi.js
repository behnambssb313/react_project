import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی


// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                ID_LA: current.password,
                LAC_NCenter:'مرکز ترک اعتیاد زندگی',
                LAC_Name:current.username,
                LAC_family: current.name,
                LAC_Ncode:current.password,
                LAC_Address:'بلوار معلم',
                LAC_Date:'۱۳۹۸',
                LAC_Date_Registration:'۱۳۹۸/۹/۲۲',
                LAC_Vist:12
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_LEAVING_ADDICTION_CENTER=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_LEAVING_ADDICTION_CENTER=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_LEAVING_ADDICTION_CENTER=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_LEAVING_ADDICTION_CENTER=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}