import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idLa',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام معتادان قطعی',
        id:'laDefinitiveName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی معتادان قطعی',
        id:'laDefinitiveFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام معتادان مشکوک',
        id:'laDubiousName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی معتادان مشکوک',
        id:'laDubiousFamily',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'شماره مکاتبه',
        id:'laCorresNumber',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'محل خدمت',
        id:'laServiceLocation',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'اقدام',
        id:'laAction',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام مرکز',
        id:'idLac',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'laVist',
        required:false,
    },
]

