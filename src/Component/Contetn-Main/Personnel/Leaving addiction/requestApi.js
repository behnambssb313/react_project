import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی


// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                key: current.id,
                ID_LA: current.password,
                LA_Definitive_Name:current.name,
                LA_Definitive_Family:current.username,
                LA_Dubious_Name: current.name,
                LA_Dubious_Family:current.username,
                LA_Corres_Number:12,
                LA_ServiceLocation:'تهران',
                LA_Action:'-',
                ID_LAC:'مرکز توانبخشی',
                Date_Registration:'۱۳۹۸/۹/۲۲',
                LA_Vist:12
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_LEAVING_ADDICTION=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_LEAVING_ADDICTION=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_LEAVING_ADDICTION=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_LEAVING_ADDICTION=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}