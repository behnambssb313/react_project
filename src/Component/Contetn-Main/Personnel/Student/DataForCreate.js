import React from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idStud',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام',
        id:'studName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'studFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی قبلی',
        id:'studLastFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام پدر',
        id:'studNfather',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'کد ملی',
        id:'studNationalCode',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'شماره شناسنامه',
        id:'studShsh',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'محل تولد',
        id:'studPlaceOfBirth',
        required:false,
    },
    {
        name:'datePicker',
        title:'تاریخ تولد',
        id:'studDateOfBirth',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'select',
        title:'میزان تحصیلات',
        id:'idEducation',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['بختیاری','لر','لک','بلوچ','ترک','گیلک','قشقایی','آشوری','ترکمن','عرب','کرد','پارس','طبری','ارمنی','تالش','گرجی',]
    },
    {
        name:'select',
        title:'رشته تحصیلی',
        id:'studMajor',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['شیمی','ریاضی']
    },
    {
        name:'radio',
        title:'جنسیت',
        id:'idGender',
        required:false,
        value:['مرد','زن']
    },
    {
        name:'radio',
        title:'وضعیت تاهل',
        id:'idMaritalStatus',
        required:false,
        value:['مجرد','متاهل']
    },
]

export const step2=[
    {
        name:'select',
        title:'دین',
        id:'studReligion',
        required:false, 
        palceholder:'انتخاب کنید...',
        value:['اسلام','مسیحیت','زرتشتی','یهودیت']
    },
    {
        name:'select',
        title:'مذهب',
        id:'studMazhab',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['شیعه','سنی','کاتولیک']
    },
    {
        name:'select',
        title:'قومیت',
        id:'studNationality',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['بختیاری','لر','لک','بلوچ','ترک','گیلک','قشقایی','آشوری','ترکمن','عرب','کرد','پارس','طبری','ارمنی','تالش','گرجی']
    },
    {
        name:'upload',
        title:'عکس',
        class:'ant-upload-list-item-name',
        id:'studImage',
        required:false,
        render:(record)=>{return <Avatar src={record} shape="square" icon="user" />}
    },
{
        name:'input',
        type:'number',
        title:'تلفن محل سکونت',
        id:'studHomePhone',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن همراه',
        id:'studMobile',
        required:false,
    },
    {
        name:'textArea',
        title:'آدرس محل سکونت',
        id:'studAddress',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'اسکان در خوابگاه',
        id:'studDorm',
        required:false,
    },
    {
        name:'textArea',
        title:'آدرس محل سکونت والدین',
        id:'studAddressP',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن محل سکونت والدین',
        id:'studHomePhoneP',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'کد پستی',
        id:'studPostalCode',
        required:false,
    },
    {
        name:'textArea',
        title:'ملاحضات',
        id:'studConcerns',
        required:false,
    },
]

export const step3=[
    {
        name:'select',
        title:'وضعیت نظام وظیفه',
        id:'studMilitaryService',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['معافیت تحصیلی','مشمول','پایان خدمت','معافیت دائم']
    },
    {
        name:'checkbox',
        title:'سابقه شرکت در تشکل دارد',
        id:'studHasOrganization',
        required:false,
    },
    {
        name:'select',
        title:'نام تشکل',
        id:'studOrganization',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['معافیت تحصیلی','مشمول','پایان خدمت','معافیت دائم']
    },
    {
        name:'checkbox',
        title:'تخلف دارد',
        id:'studHasInfringement',
        required:false,
    },
    {
        name:'textArea',
        title:'موضوع تخلف',
        id:'studInfringement',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام لاتین',
        id:'studNameL',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'شماره پاسپورت',
        id:'studPassportNumber',
        required:false,
    },
    {
        name:'upload',
        title:'تصویر پاسپورت',
        class:'ant-upload-list-item-name',
        id:'studPassportPicture',
        required:false,
        render:(record)=>{return <Avatar src="https://varzesh3.com" shape="square" icon="user" />}
    },
    {
        name:'checkbox',
        title:'مسلط به زبان فارسی',
        id:'studFarsi',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'studVisit',
        required:false,
    },
]