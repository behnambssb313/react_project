import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                key: current.id,
                ID_STUD: current.password,
                STUD_Name:current.name,
                STUD_Family:current.username,
                STUD_LastFamily: current.username,
                STUD_NFather:'ali',
                STUD_SHSH:current.address.number,
                STUD_PlaceOfBirth:current.address.city,
                STUD_DateOfBirth:'۱۳۹۸/۹/۲۲',
                ID_Education:'لیسانس',
                STUD_Major:'پرورش کرم های کودی',
                ID_Gender:1,
                ID_maritalStatus:2,
                


                
                STUD_Religion:'اسلام',
                STUD_Mazhab:'شیعه',
                STUD_Nationality:'بلوچ',
                STU_Image :'4.jpg',
                STUD_HomePhone:'02155448877',
                STUD_Mobile:'91259595959',
                STUD_Address:current.address.street,
                STUD_Dorm:'-',
                STUD_AddressP:'شهرکرد',
                STUD_HomePhoneP:'02166242529',
                STUD_PostalCode:'13766958420',
                STUD_Concerns:'_',

                STUD_MilitaryService:'معافیت تحصیلی',
                STUD_HasOrganization:'1',
                STUD_Organization:'تشکل حمایتی',
                
                STUD_HasInfringement:'1',
                STUD_infringement:'سیگار کشیدن',

                STUD_NameL:'behnam Sarkhosh',
                STUD_Passport_Number:'1252948',
                STUD_Passport_Picture:'458.png',
                STUD_Farsi:'1',

                STUD_Date_Registration:'۱۳۹۸/۹/۲۲',
                STUD_Visit:18
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_STUDENT=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_STUDENT=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_STUDENT=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_STUDENT=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}