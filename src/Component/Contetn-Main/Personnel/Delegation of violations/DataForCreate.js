import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idDov',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام صاحب رای',
        id:'dovName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی صاحب رای',
        id:'dovFamily',
        required:false
    },
    {
        name:'textArea',
        title:'علت صدور رای',
        id:'dovCause',
        required:false
    },
    {
        name:'textArea',
        title:'نتیجه رای ',
        id:'dovResult',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'تاریخ',
        id:'dovDate',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'محل خدمت',
        id:'dovServiceLocation',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'dovVist',
        required:false,
    },
]

