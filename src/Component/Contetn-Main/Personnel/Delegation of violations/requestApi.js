import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)  
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                ID_DOV: current.password,
                DOV_Name:current.username,
                DOV_Family:current.name,
                DOV_Cause:'-',
                DOV_Result:'-',
                DOV_Date:'۱۳۹۸/۹/۲۲',
                DOV_ServiceLocation:'شهرکرد',
                DOV_Date_Registration:'۱۳۹۸/۹/۲۲',
                DOV_Vist:12
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_DELEGATION_OF_VIOLATIONS=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_DELEGATION_OF_VIOLATIONS=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_DELEGATION_OF_VIOLATIONS=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_DELEGATION_OF_VIOLATIONS=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}
