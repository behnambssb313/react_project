import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                idSsaf: current.id,
                ssafName:current.name,
                ssafFamily:current.username,
                ssafLastFamily: current.username,
                ssafNfather:'ali',
                safNationalCode:current.password,
                ssafShsh:current.address.number,
                ssafDateOfBirth:current.address.city,
                ssafPlaceOfBirth:'۱۳۹۸/۹/۲۲',
                idEducation:'لیسانس',
                idGender:1,
                idMaritalStatus:2,
                idEmploymentStatus:'قراردادی',
                idResponsibility:'مدیر',
                ssafWorkPhone:'02166242529',
                ssafHomePhone:'02155448877',
                ssafMobile:'91259595959',
                ssafAddress:current.address.street,
                ssafDescription:'_',

                ssaf_ServiceLocation:'تهران',
                ssafSh:'ستاد',
                ssafNOfStaff:12, // check Name in api
                ssaftateOfSacrifice:'-',
                ssafYOSISecurity:'-', // check Name in api

                ssafConfirmation:1,
                ssafShocker:2,
                ssafTrainingCourses:1,
                ssafDateRegistration:'۱۳۹۸/۹/۲۲',
                ssafVisit:12
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_SECURITY_STAFF=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>{
            response.data.map(current=>dataSource.push(current))   
          })
    return dataSource;  
}


// postData to MedicalScience_API
export const POST_SECURITY_STAFF=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_SECURITY_STAFF=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_SECURITY_STAFF=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}