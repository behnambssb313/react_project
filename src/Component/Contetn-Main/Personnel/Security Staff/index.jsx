import React,{Component} from 'react';
import { Table, Form,Row,Col,Button,Input,Card } from 'antd';
import { connect } from "react-redux";

import CreateAndSettingTable from './CreateAndSettingTable';
import DataSource  from "./Columns.js";
import {GET_FAKE,GET_SECURITY_STAFF} from "./requestApi";


// colums table
const {Search}=Input;
const pagination = { position: 'bottom' };


class SecurityStaff extends Component {
  constructor(props){
    super(props);
  }

  state = {
    pagination,
    hasData: true,
    display:'none',
  };

  /// RequestApi: Get
    componentDidMount(){
      const datas=GET_FAKE('https://jsonplaceholder.ir/users');
      const datass=GET_SECURITY_STAFF('');
      this.props.dataSave(datas);
    }

  // settings table evant
    toggle = () => {
      if(this.state.display=='none'){
        return this.setState({display:'block' })  
      }
      return this.setState({display:'none' })  
    };

  render() {
    return (
          <div>
            <DataSource props={this.props} drawerCreateData={this.props.drawerCreateData}/>
              <Row type="flex" justify="center" style={{marginTop:'80px'}}>
                <Col span={24} style={{textAlign:'right'}}>
                  <CreateAndSettingTable/>

                  {/* <Search placeholder="بگرد دنبال ..." onSearch={value => console.log(value)} size="large" key="1"/>
                  <Card style={{display:`${this.state.display}`}}>
                    <p style={{textAlign:'right',fontSize:'16px'}}>جستجو براساس :</p>
                    <Row type="flex" justify="space-around">
                      <Col span={7}>
                        <Form.Item>
                          
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                  <Button size={"large"} type="primary" key={1} key="2" onClick={this.toggle} block>جستجوی پیشرفته</Button> */}

                  <Table style={{marginTop:10}} dataSource={this.props.state.data}  rowClassName={() => 'editable-row'} {...this.props.state} scroll={{ x: 5000 }} bordered/> 
                </Col>  
              </Row>
          </div>
    );
  }
}


const mapStateToProps=(state)=>{
  return{
    state
  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    dataSave:(payload)=>{
      dispatch({                              
        type:'DATA',
        payload
      })
    }
  }
}

const   WrappedNormalContentMain = Form.create({ name: 'SecurityStaff' })(SecurityStaff);
export default connect(mapStateToProps,mapDispatchToProps)(WrappedNormalContentMain);  