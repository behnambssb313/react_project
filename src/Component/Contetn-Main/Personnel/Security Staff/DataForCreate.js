import React from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idSsaf',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام',
        id:'ssafName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'ssafFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی قبلی',
        id:'ssafLastFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام پدر',
        id:'ssafNfather',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'کد ملی',
        id:'ssafNationalCode',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'شماره شناسنامه',
        id:'ssafShsh',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'محل تولد',
        id:'ssafDateOfBirth',
        required:false,
    },
    {
        name:'datePicker',
        title:'تاریخ تولد',
        id:'ssafPlaceOfBirth',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'select',
        title:'میزان تحصیلات',
        id:'idEducation',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['زیر دیپلم','دیپلم','فوق دیپلم','لیسانس','فوق لیسانس','دکترا']
    },
    {
        name:'radio',
        title:'جنسیت',
        id:'idGender',
        required:false,
        value:['مرد','زن'],
    },
    {
        name:'radio',
        title:'وضعیت تاهل',
        id:'idMaritalStatus',
        required:false,
        value:['مجرد','متاهل'],
    },
]

export const step2=[
    {
        name:'input',
        type:'text',
        title:'وضعیت استخدام',
        id:'idEmploymentStatus',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'سمت',
        id:'idResponsibility',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'محل خدمت',
        id:'ssafServiceLocation',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'کارمند واحد تابعه یا ستاد',
        id:'ssafSh',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن محل کار',
        id:'ssafWorkPhone',
        required:false,
    },
]

export const step3=[
    {
        name:'checkbox',
        title:'تاییدیه حراست کل دارد',
        id:'ssafConfirmation',
        required:false,
    },
    {
        name:'checkbox',
        title:'افشانه/شوکر در اختیار دارد',
        id:'ssafShocker',
        required:false,
    },
    {
        name:'checkbox',
        title:'دوره های آموزشی حراستی را گذرانده',
        id:'ssafTrainingCourses',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تعداد پرسنل',
        id:'ssafNOfStaff',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'سنوات خدمتی در حراست',
        id:'ssafYOSISecurity',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'وضعیت ایثارگری',
        id:'ssafStateOfSacrifice',
        required:false,
    },
    {
        name:'textArea',
        title:'توضیحات',
        id:'ssafDescription',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'تلفن محل سکونت',
        id:'ssafHomePhone',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن همراه',
        id:'ssafMobile',
        required:false,
    },
    {
        name:'textArea',
        title:'آدرس محل سکونت',
        id:'ssafAddress',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'ssafVisit',
        required:false,
    }
]