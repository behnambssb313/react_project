import Staff from './Staff';
import Student from "./Student";
import SecurityStaff from "./Security Staff";
import LeaviddAiction from "./Leaving addiction";
import LeavingAddictionCenter from "./Leaving addiction Center";
import DelegationOfViolations from "./Delegation of violations";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";

const personnelRoutes=[
    {
        path:'/Personnel/Staff',
        component:Staff,
    },
    {
        path:'/Personnel/Security-Staff',
        component:SecurityStaff,
    },
    {
        path:'/Personnel/Leaving-Addiction',
        component:LeaviddAiction,
    },
    {
        path:'/Personnel/Leaving-Addiction-Center',
        component:LeavingAddictionCenter,
    },
    {
        path:'/Personnel/Student',
        component:Student,
    },
    {
        path:'/Personnel/Delegation-Of-Violations',
        component:DelegationOfViolations,
    },
    {
        path:'/Personnel/Companies',
        component:Companies,
    },
    {
        path:'/Personnel/Manuals',
        component:Manuals,
    },
    {
        path:'/Personnel/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/Personnel/Visit',
        component:Visit,
    },
];


export default personnelRoutes;
