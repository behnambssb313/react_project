import React,{Component} from 'react';
import { Table, Form,Row,Col,Button,Input,Card } from 'antd';
import { connect } from "react-redux";

import CreateAndSettingTable from './CreateAndSettingTable';
import DataSource  from "./Columns.js";
import {GET_FAKE,GET_STAFF} from "./requestApi";

import Piechart from "../../../Utilities/Chart";

// colums table
const {Search}=Input;
const pagination = { position: 'bottom' };


const datas=GET_FAKE('https://jsonplaceholder.ir/users'); //
class Staff extends Component {
  constructor(props){
    super(props);
  }

  state = {
    pagination,
    hasData: true,
    display:'none',
    data:datas
  };


  /// RequestApi
    componentDidMount(){
      this.props.dataSave(this.state.data);
    }

  // settings table evant
    toggle = () => {
      if(this.state.display=='none'){
        return this.setState({display:'block' })  
      }
      return this.setState({display:'none' })  
    };

  render() {
    return (
          <div>
            <DataSource props={this.props} drawerCreateData={this.props.drawerCreateData}/>
              <Row type="flex" justify="center">
                <Col span={24} style={{textAlign:'right'}}>
                <Row>
                  <Col span={12}><h4>آمار جنسیتی </h4
                  ><Piechart/></Col>
                  <Col span={12}>
                    
                  </Col>
                </Row>
                <CreateAndSettingTable/>
                
                  {/* <Search placeholder="بگرد دنبال ..." onSearch={value => console.log(value)} size="large" key="1"/>
                  <Card style={{display:`${this.state.display}`}}>
                    <p style={{textAlign:'right',fontSize:'16px'}}>جستجو براساس :</p>
                    <Row type="flex" justify="space-around">
                      <Col span={7}>
                        <Form.Item>
                          
                        </Form.Item>
                      </Col>
                    </Row>
                  </Card>
                  <Button size={"large"} type="primary" key={1} key="2" onClick={this.toggle} block>جستجوی پیشرفته</Button> */}

                  <Table  style={{marginTop:10,boxShadow:'0 2px 1px rgba(0, 0, 0, 0.05)'}} dataSource={this.state.data}  rowClassName={() => 'editable-row'} {...this.props.state} scroll={{ x: 6000 }} bordered/> 
                </Col>  
              </Row>
          </div>
    );
  }
}


const mapStateToProps=(state)=>{
  return{
    state
  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    dataSave:(payload)=>{
      dispatch({                              
        type:'DATA',
        payload
      })
    }
  }
}

const   WrappedNormalContentMain = Form.create({ name: 'Staff' })(Staff);
export default connect(mapStateToProps,mapDispatchToProps)(WrappedNormalContentMain);  