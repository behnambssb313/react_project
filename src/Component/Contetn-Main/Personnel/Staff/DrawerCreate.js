import React,{Component} from 'react';
import { Form,Row,Drawer  } from 'antd';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import {step1,step2,step3} from "./DataForCreate";
import { connect } from "react-redux";

import { POST_STAFF } from "./requestApi";

import  {
        ConnectedInputGenerator,
        ConnectedDatePickerGenerator,
        ConnectedRadioGenerator,
        ConnectedSelectGenerator,
        ConnectedAutoComplateGenerator,
        ConnectedTextArea,
        ConnectedUploadGenerator,
        ConnectedCheckboxGenerator
        } from "../../../Utilities/inputGenerator";


class DrawerCreate extends Component{
    constructor(props){
        super(props);
        this.state={
            visible: false, childrenDrawer: false ,childrenDrawer2: false,selectedDay: null
        }
    }
    showDrawer = () => {
        this.setState({
          visible: true,
        });
      };
    
      onClose = () => {
        this.setState({
          visible: false,
        });
      };
    
      showChildrenDrawer = () => {
        this.setState({
          childrenDrawer: true,
        });
      };
    
      onChildrenDrawerClose = () => {
        this.setState({
          childrenDrawer: false,
        });
      };

      showChildrenDrawer2 = () => {
        this.setState({
          childrenDrawer2: true,
        });
      };
    
      onChildrenDrawerClose2 = () => {
        this.setState({
          childrenDrawer2: false,
        });
      };

      handleSubmitDrawer1 = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err) => {
          if (!err) {
            this.showChildrenDrawer()
          }
        });
      };

      handleSubmitDrawer2 = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err) => {
          if (!err) {
            this.showChildrenDrawer2()
          }
        });
      };

      handleSubmitDrawer3 = e => {
        e.preventDefault();
        console.log('s',this.props.state.input)
        POST_STAFF('https://localhost:44341/api/StaffApi',this.props.state.input)
      };

  render() {
    return (
      <div>
        <Form layout="inline" className="login-form" >
          <Row type="flex" justify="start">
            {step1.map((item)=>{
                  switch (item.name) {
                    case 'input':
                      return <ConnectedInputGenerator value='' props={this.props} data={item}/>
                    case 'datePicker':
                      return <ConnectedDatePickerGenerator value='' props={this.props} data={item}/>
                    case 'radio':
                      return <ConnectedRadioGenerator value='' data={item} props={this.props}/>
                    case 'select':
                    return <ConnectedSelectGenerator  props={this.props} data={item}/>
                    default:
                      break;
                  }
            })}
          </Row>
          <button class="btn btn-success" htmlType="submit" style={{float:'left'}} onClick={this.handleSubmitDrawer1}>تکمیل اطلاعات </button>
        </Form>
          <Drawer
            title="مرحله دوم (اطلاعات محل کار)"
            width={720}
            height={600}
            onClose={this.onChildrenDrawerClose}
            visible={this.state.childrenDrawer}
            placement={'bottom'}
            style={{textAlign:'right',direction:'rtl'}}
          >
            <Form layout="inline" className="login-form">
              <Row type="flex" justify="start">
                {step2.map((item)=>{
                  switch (item.name) {
                    case 'input':
                      return <ConnectedInputGenerator value='' props={this.props} data={item}/>
                    case 'autoComplate':
                      return <ConnectedAutoComplateGenerator value='' props={this.props} data={item}/>
                      case 'datePicker':
                      return <ConnectedDatePickerGenerator value='' props={this.props} data={item}/>
                      case 'checkbox':
                          return <ConnectedCheckboxGenerator value='' props={this.props} data={item}/>
                          case 'select':
                            return <ConnectedSelectGenerator  props={this.props} data={item}/>
                    default:
                      break;
                  }
                })}
              </Row>
              <button class="btn btn-success" style={{float:'left'}} onClick={this.handleSubmitDrawer2}>تکمیل اطلاعات </button>
            </Form>
              <Drawer
                title="مرحله سوم (تکمیل اطلاعات)"
                width={720}
                height={600}
                onClose={this.onChildrenDrawerClose2}
                visible={this.state.childrenDrawer2}
                placement={'bottom'}
                style={{textAlign:'right',direction:'rtl'}}
              >
                <Form layout="inline" className="login-form">
                  <Row type="flex" justify="start">
                    {step3.map(item=>{
                      switch (item.name) {  
                        case 'input':
                          return <ConnectedInputGenerator value='' data={item} props={this.props} />
                        case 'upload':
                          return <ConnectedUploadGenerator value='' data={item} props={this.props} />
                        case 'select':
                          return <ConnectedSelectGenerator  props={this.props} data={item}/>
                        case 'textArea':
                          return <ConnectedTextArea value='' props={this.props} data={item}/>
                        default:
                          break;
                      }
                    })}
                  </Row>
                  <button class="btn btn-success" style={{float:'left'}} onClick={this.handleSubmitDrawer3}>ثبت اطلاعات
                  </button>   
                </Form>         
              </Drawer> 
          </Drawer>
      </div>
     );
  }
}
const mapStateToProps=(state)=>{
  return{
    state
  }
}

const mapDispatchToProps=(dispatch)=>{
  return{}
}

const wrappedStaff = Form.create({ name: 'Staff' })(DrawerCreate);
export default connect(mapStateToProps,mapDispatchToProps)(wrappedStaff);


