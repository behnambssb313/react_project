import React,{Component} from 'react';
import {Icon,Button,Input,Popconfirm } from 'antd';
import Highlighter from "react-highlight-words";

import { connect } from "react-redux";
import {step1,step2,step3} from "./DataForCreate";
import ModalEdit from "./ModalEdit";


import { PUT_STAFF } from "./requestApi";

class Columns extends Component {
  constructor(props){
    super(props);
  }
  state={
    editingKey:'',
    editable:false,
    filteredInfo: null,
    sortedInfo: null,
  }  

handleChange = (pagination, filters, sorter) => {
  // console.log('Various parameters', pagination, filters, sorter);
  this.setState({
    filteredInfo: filters,
    sortedInfo: sorter,
  });
};

clearFilters = () => {
  this.setState({ filteredInfo: null });
};

clearAll = () => {
  this.setState({
    filteredInfo: null,
    sortedInfo: null,
  });
};

setAgeSort = () => {
  this.setState({
    sortedInfo: {
      order: 'descend',
      columnKey: 'age',
    },
  });
};
  // search columns
  getColumnSearchProps = (dataIndex,title) => ({
    
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
     
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`جستجوی ${title}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{  width:188, marginBottom: 8, display: 'block' }}
        />
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{  width:90 }}>
          بازگشت
        </Button>
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          size="small"
          style={{ width:90, marginLeft: 8 }}
        >
          جستجو
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
     
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.props.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.props.setSearchText(selectedKeys[0])
  };

  handleReset = clearFilters => {
    clearFilters();
    this.props.setSearchText('')
  };

  // delete records
  handleDelete = key => {
    const filtering=this.props.data.filter(item => item.idSaf !== key);
    this.props.dataUpdate(filtering)
  };

   /// edit
   isEditing = record => record.idSaf === this.state.editingKey;

   cancel = () => {
     this.setState({editingKey:''});
     this.props.setEditingKey('');
   };
 
   save(form, key) {
    console.log('this',this.props.state.input)
     form.validateFields((error, row) => {
       const newData = [...this.props.data];
       const index = newData.findIndex(item => key === item.idSaf);
       if (index > -1) {
         const item = newData[index];
         newData.splice(index, 1, {
           ...item,
           ...this.props.state.input,
         });
         console.log('updateData',newData)
         this.props.dataUpdate(newData);
         this.setState({editingKey:''});
         this.props.setEditingKey('');
       } else {
         newData.push(row);
         this.props.dataUpdate(newData);
         this.setState({editingKey:''});
         this.props.setEditingKey('');
       }
     });
   }
 
   edit(key) {
     this.setState({editingKey:key});
     this.props.setEditingKey(key);
   }

  componentDidMount(){
    const arrayContact=[];
    const result=arrayContact.concat(step1,step2,step3);
    let columns=[];
    result.map(item=>{
      columns.push({
      ...item,
      dataIndex:item.id,
      align:'center',
      editable:true,
      sorter: (a, b) => a[item.id].length - b[item.id].length,
      sortDirections: ['descend', 'ascend'],
      ...this.getColumnSearchProps(item.id,item.title),
      render:(record)=>{
        if(item.name=='upload'){
          return record
        }
        else if(item.name=='radio'){
          return item.title=='جنسیت' ? (record==1 ? 'مرد' : 'زن') : (record==1 ? 'مجرد' : 'متاهل')
        }
        else{
          return record
        }
      }
      })
    })
      columns.push(
        {
          title: 'فعالیت ها',
          dataIndex:'action',
          align:'center',
          fixed:'left',
            
          editable:false,
          render: (text, record) => {
            console.log('behnam',record)
            const editable = this.isEditing(record);
            return editable ? (
              <ModalEdit
              recording={record}
              columnsing={this.props.columns}
              propsss={this.props.props} 
              onClicking={() => this.save(this.props.props.form, record.idSaf)}
              onConfirming={() => this.cancel(record.idSaf)}
              />
            ) : (
              this.props.data.length >= 1 ? (
                <Popconfirm title="آیا مطمئن هستید؟" onConfirm={() => this.handleDelete(record.idSaf)} >
                  <a href="#/"><Icon type="delete"/></a>
                  
                  <a href="#/" disabled={this.state.editingKey !== ''} onClick={() =>this.edit(record.idSaf)}><Icon type="edit"/></a>
                </Popconfirm>
              ) : null
            )
          },
        }
    )
    this.props.setColumns(columns)
  }  
  render() {
    return (
      <></>
    )
  }
}


const mapStateToProps=(state)=>{
  return{
    state,
    data:state.data,
    columns:state.columns,
    searchText:state.searchText,
    editingKey:state.editingKey,
    editingg:state.editingg,
    drawerCreateData:state.drawerCreateData

  }
}
const mapDispatchToProps=(dispatch)=>{
  return{
    setColumns:(payload)=>{
      dispatch({
        type:'SET_COLUMNS',
        payload
      })
    },
    dataUpdate:(payload)=>{
      dispatch({
        type:'DATA',
        payload
      })
    },
    setSearchText:(payload)=>{
      dispatch({
        type:'SETSEARCHTEXT',
        payload
      })
    },
    setEditingKey:(payload)=>{
      dispatch({
        type:'SETEDITINGKEY',
        payload
      })
    },
    setEditing:(payload)=>{
      dispatch({
        type:'SETEDITING',
        payload
      })
    }
    
  }
}
const DataSource=connect(mapStateToProps,mapDispatchToProps);
export default DataSource(Columns)