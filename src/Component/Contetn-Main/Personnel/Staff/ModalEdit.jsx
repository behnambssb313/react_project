import React, { useState,useEffect } from 'react'
import {Modal,Form,Row,Button } from 'antd';
import { connect } from "react-redux";
import {
        ConnectedInputGenerator,
        ConnectedDatePickerGenerator,
        ConnectedRadioGenerator,
        ConnectedSelectGenerator,
        ConnectedAutoComplateGenerator,
        ConnectedTextArea,
        ConnectedUploadGenerator
        } from "../../../Utilities/inputGenerator";


function ModalEdit({recording,columnsing,propsss,onClicking,onConfirming,setCurrentRecord}) {

    const [visible, setVisible] = useState(true)
    const [loading, setLoading] = useState(false)

    const handleOk = e => {
      setLoading(true)
      setTimeout(() => {
        setLoading(false)
        setVisible(false);
        onClicking();
      }, 2000);
    };

    useEffect(() => {
      console.log('recording',recording)
      setCurrentRecord(recording)
    }, [])
  
    const handleCancel = e => {
      setVisible(false);
      onConfirming()
    };

    return (
        <Modal
          width={1880}
          style={{height:'1000px'}}
          visible={visible} 
          onOk={(e)=>handleOk(e)}
          onCancel={(e)=>handleCancel(e)} 
          footer={[
            <Button className="btn btn-success" htmlType="submit" key="submit" loading={loading} onClick={(e)=>handleOk(e)}>
              ثبت
            </Button>,
            <Button className="btn btn-danger" key="back" onClick={(e)=>handleCancel(e)}>
              لغو
            </Button>
          ]}
        >
          <Form layout="inline" className="login-form" >
            <Row type="flex" justify="start">
              {            
                columnsing.map(item=>{
                  let values=recording[item.dataIndex];
                      switch (item.name) {
                        case 'input':
                          return <ConnectedInputGenerator value={values}  props={propsss} data={item}/>
                        case 'datePicker':
                          return <ConnectedDatePickerGenerator value={values} props={propsss} data={item}/>
                        case 'radio':
                          return <ConnectedRadioGenerator value={values} data={item} props={propsss}/>
                        case 'select':
                          return <ConnectedSelectGenerator value={values} data={item} props={propsss}/>
                        case 'autoComplate':
                          return <ConnectedAutoComplateGenerator value={values} props={propsss} data={item}/>
                        case 'upload':
                          return <ConnectedUploadGenerator value={values}  props={propsss} data={item}/>
                        case 'textArea':
                          return <ConnectedTextArea value={values} props={propsss} data={item}/>
                        default:
                          break;
                      }
                })
              }
            </Row>
          </Form>
        </Modal>
    )
}


const mapStateToProps=(state)=>{
  return{}
}
const mapDispatchToProps=(dispatch)=>{
  return{
   setCurrentRecord:(payload)=>{
    dispatch({
      type:'SET_CURRENT_RECORD',
      payload
    })
   }
  }
}


export default connect(mapStateToProps,mapDispatchToProps)(ModalEdit) 