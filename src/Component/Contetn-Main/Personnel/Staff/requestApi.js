import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)
         .then(response=>{
  
            response.data.map((current)=>{
             
                dataSource.push({
                key: current.id,
                idSaf:current.id,
                safName:current.name,
                safFamily:current.username,
                safLastFamily: current.username,
                safNfather:'ali',
                safNationalCode:'12',
                safShsh:current.address.number,
                safPlaceOfBirth:current.address.city,
                safDateOfBirth:'۱۳۹۸/۹/۲۲',
                idEducation:'لیسانس',
                idGender:1,
                idMaritalStatus:2,

                safAssistance:'تهران',
                safAdministration:'شهرکرد',
                safWorkPhone:'02166242529',
                idResponsibility:'کارمند',
                idEmploymentStatus:'قراردادی',
                safSchoolOfTeaching:'true',

                safUpdateHistory:'۱۳۹۸/۹/۲۲',

                safTypeOfIssue:'-',
                safExperimentDate:'۱۳۹۸/۹/۲۲',
                safTypeOfDrug:'-',
                safHowToIdentify:'-',
                safWrittenCommitment:'-',
                safTrustedForce:1,
                safDescriptionTR:'-',
                safDateregistration:'۱۳۹۸/۹/۲۲',
                safVisit:12,

                safImageSh:'4.jpg',
                safImage:'5.jpg',
                safNationalCardImage:'6.jpg',
                safPersonnelardImage:'8.jpg',
                idNationality:'بلوچ',
                idReligion:'اسلام',
                idMazhab:'شیعه',
                safDescription:'_',
                safHomePhone:'02155448877',
                safMobile:'91259595959',
                safAddress:current.address.street
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_STAFF=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_STAFF=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_STAFF=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_STAFF=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}