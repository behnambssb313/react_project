import React from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idSaf',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام',
        id:'safName',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'safFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی قبلی',
        id:'safLastFamily',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نام پدر',
        id:'safNfather',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'کد ملی',
        id:'safNationalCode',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'شماره شناسنامه',
        id:'safShsh',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'محل تولد',
        id:'safPlaceOfBirth',
        required:false,
    },
    {
        name:'datePicker',
        title:'تاریخ تولد',
        id:'safDateOfBirth',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'select',
        title:'میزان تحصیلات',
        id:'idEducation',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['زیر دیپلم','دیپلم','فوق دیپلم','لیسانس','فوق لیسانس','دکترا']
    },
    {
        name:'radio',
        title:'جنسیت',
        id:'idGender',
        required:false,
        value:['مرد','زن'],
    },
    {
        name:'radio',
        title:'وضعیت تاهل',
        id:'idMaritalStatus',
        required:false,
        value:['مجرد','متاهل'],
    },
]

export const step2=[
    {
        name:'autoComplate',
        type:'text',
        title:'محل کار معاونت',
        id:'safAssistance',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'text',
        title:'محل کار اداره کل',
        id:'safAdministration',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن محل کار',
        id:'safWorkPhone',
        required:false,
    },
    {
        name:'select',
        title:'سمت',
        id:'idResponsibility',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['زیر دیپلم']
    },
    {
        name:'select',
        title:'وضعیت استخدام',
        id:'idEmploymentStatus',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['زیر دیپلم']
    },
    {
        name:'input',
        type:'text',
        title:'تعرفه حراست',
        id:'safSchoolOfTeaching',
        required:false,
    },
    {
        name:'datePicker',
        title:'آخرین تاریخ به روزرسانی تعرفه حراست',
        id:'safUpdateHistory',
        required:false,
        palceholder:'انتخاب کنید...'
    },  
    {
        name:'input',
        type:'text',
        title:'نوع مساله',
        id:'safTypeOfIssue',
        required:false,
    },  
    {
        name:'datePicker',
        title:'تاریخ آزمایش',
        id:'safExperimentDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'نوع مواد مخدر',
        id:'safTypeOfDrug',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'نحوه شناسایی',
        id:'safHowToIdentify',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'اخذ تعهد کتبی',
        id:'safWrittenCommitment',
        required:false,
    },
    {
        name:'checkbox',
        title:'نیرو معتمد و توانمند سیستم',
        id:'safTrustedForce',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'توضیحات نیرو معتمد',
        id:'safDescriptionTR',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'safVisit',
        required:false,
    },
]

export const step3=[
    {
        name:'upload',
        title:'تصویر شناسنامه',
        class:'ant-upload-list-item-name',
        id:'safImageSh',
        required:false,
    },
    {
        name:'upload',
        title:'عکس',
        class:'ant-upload-list-item-name',
        id:'safImage',
        required:false,
        render:(record)=>{return <Avatar src={record} shape="square" icon="user" />}
    },
    {
        name:'upload',
        title:'تصویر کارت ملی',
        class:'ant-upload-list-item-name',
        id:'safNationalCardImage',
        required:false,
    },
    {
        name:'upload',
        title:'تصویر کارت پرسنلی',
        class:'ant-upload-list-item-name',
        id:'safPersonnelardImage',
        required:false,
    },
    {
        name:'select',
        title:'دین',
        id:'idReligion',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['اسلام','مسیحیت','زرتشتی','یهودیت']
    },
    {
        name:'select',
        title:'مذهب',
        id:'idMazhab',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['شیعه','سنی','کاتولیک']
    },
    {
        name:'select',
        title:'قومیت',
        id:'idNationality',
        required:false,
        palceholder:'انتخاب کنید...',
        value:['بختیاری','لر','لک','بلوچ','ترک','گیلک','قشقایی','آشوری','ترکمن','عرب','کرد','پارس','طبری','ارمنی','تالش','گرجی']
    },
    {
        name:'textArea',
        title:'توضیحات',
        id:'safDescription',
        required:false,
    },
    {
        name:'input',
        type:'text',
        title:'تلفن محل سکونت',
        id:'safHomePhone',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تلفن همراه',
        id:'safMobile',
        required:false,
    },
    {
        name:'textArea',
        title:'آدرس محل سکونت',
        id:'safAddress',
        required:false,
    }
]