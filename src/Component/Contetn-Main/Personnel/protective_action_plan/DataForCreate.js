import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idPap',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'فیلد های اجرا شده در ماه',
        id:'papFEIM',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'موضوعات اجرایی حفاظت اسناد',
        id:'papIITEOD',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'حفاظت پرسنلی',
        id:'papPersonnelProtectio',
        required:false
    },
    {
        name:'textArea',
        title:'فناوری اطلاعات',
        id:'papIt',
        required:false
    },
    {
        name:'textArea',
        title:'موضوعات حفاظتی فیزیکی',
        id:'papPPI',
        required:false
    },
    {
        name:'textArea',
        title:'موضوعات وضعیت پیشرفت',
        id:'papPSI',
        required:false
    },
    {
        name:'textArea',
        title:'نواقص',
        id:'papDefects',
        required:false
    },
    {
        name:'textArea',
        title:'علت نقص',
        id:'papCauseOfDefect',
        required:false,
    },
]

