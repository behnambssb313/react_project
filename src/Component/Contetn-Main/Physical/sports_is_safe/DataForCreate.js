import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'textArea',
        title:'اقدامات ورزشی',
        id:'sisSportActivities',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'تعداد',
        id:'sisNumber',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ برگزاری',
        id:'sisDateHeld',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'visVist',
        required:false,
    },
]

