import PassiveDefense from "./passive_defense";
import SportsIsSafe from "./sports_is_safe";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";


const PhysicalRoutes=[
    {
        path:'/Physical/Passive-Defense',
        component:PassiveDefense,
    },
    {
        path:'/Physical/Sports-Is-Safe',
        component:SportsIsSafe,
    },
    {
        path:'/Physical/Companies',
        component:Companies,
    },
    {
        path:'/Physical/Manuals',
        component:Manuals,
    },
    {
        path:'/Physical/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/Physical/Visit',
        component:Visit,
    },
];


export default PhysicalRoutes;
