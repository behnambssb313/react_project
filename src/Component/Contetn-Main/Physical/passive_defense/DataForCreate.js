import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'datePicker',
        title:'تاریخ جلسه',
        id:'pdDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'شماره نامه',
        id:'pdNLetter',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'موضوع',
        id:'pdsubject',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'واحد',
        id:'idUnit',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'textArea',
        title:'توضیحات',
        id:'pdActions',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'pdVist',
        required:false,
    },
]

