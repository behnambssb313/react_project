import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'تعداد',
        id:'suiNumber',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام فرد',
        id:'suiName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی',
        id:'suiFamilyP',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'شهر',
        id:'idCity',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'datePicker',
        title:'تاریخ خودکشی',
        id:'suiDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'نام واحد درمانگر',
        id:'suiTherapistUnit',
        required:false
    },
    {
        name:'autoComplate',
        type:'text',
        title:'وضعیت نهایی',
        id:'idFinalStatus',
        required:false,
        data:[{title: 'تهران',},{title: 'مشهد',},],
    },
    {
        name:'input',
        type:'number',
        title:'شماره مکاتبه',
        id:'suiNCorres',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'suiVist',
        required:false,
    },
]

