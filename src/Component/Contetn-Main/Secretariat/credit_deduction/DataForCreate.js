import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'شماره',
        id:'idCd',
        required:true,
    },
    {
        name:'input',
        type:'number',
        title:'نام واحد',
        id:'idUnit',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'مبلغ کسری',
        id:'cdAmountDeduction',
        required:false
    },
    {
        name:'textArea',
        title:'علت کسری',
        id:'cdFamilyP',
        required:false
    },
    {
        name:'textArea',
        title:'مکاتبات',
        id:'cdCorres',
        required:false
    },
    {
        name:'textArea',
        title:'اقدامات',
        id:'cdActions',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'cdVist',
        required:false,
    },
]

