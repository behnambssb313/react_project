import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idCp',
        required:true,
    },
    {
        name:'input',
        type:'number',
        title:'نام شاکی',
        id:'cpNameP',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی شاکی',
        id:'cpFamilyP',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی شاکی',
        id:'cpNcodeP',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره تماس',
        id:'cpNPhone',
        required:false
    },
    {
        name:'textArea',
        title:'آدرس',
        id:'cpAddress',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام متشاکی',
        id:'cpNameC',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی متشاکی',
        id:'cpFamilyC',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'کد ملی متشاکی',
        id:'cpNCodeC',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره تماس',
        id:'cpNPhoneC',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'واحد محل کار',
        id:'cpWorkplace',
        required:false
    },
    {
        name:'textArea',
        title:'علت شکایت',
        id:'cpComplaint',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نتیجه شکایت',
        id:'cpData',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'cpVisit',
        required:false,
    },
]

