import axios from "axios";
/// یک نظریه برای اینکه از این فایل کلا یدونه داشته باشیم به این شکل که فقط آدرس رو پاس بدیم و بقیه کارا با خودش انجام بشه این است که: یه مپ دیگه رو ابجکت جاری بزنیم که مقدار کی و ولیو خودکار تعیین بشه نه دستی



// getData from jsonplaceholder.ir
export const GET_FAKE=(url)=>{
    let dataSource=[];
    axios.get(url)  
         .then(response=>{
           
            response.data.map((current)=>{
              // console.log('response.data',current)
                dataSource.push({
                idCp: current.password,
                cpNameP:current.name,
                cpFamilyP:current.username,
                cpNcodeP:1212,
                cpNPhone:'12121',
                cpAddress:'-',
                cpNameC:current.name,
                cpFamilyC:current.username,
                cpNCodeC:1212,
                cpNPhoneC:'091254878',
                cpWorkplace:'-',
                cpComplaint:'-',
                cpData:'-',
                cpVisit:12,
              })
            })
            
          })
          return dataSource;  
}


// getData from MedicalScience_API
export const GET_COMPLAINING_PEOPLE=(url)=>{
  let dataSource=[];
    axios.get(url)
         .then(response=>
            response.data.map(current=>dataSource.push(current))
          )
  return dataSource;  
}


// postData to MedicalScience_API
export const POST_COMPLAINING_PEOPLE=(url,data)=>{

  axios.post(url,data)
       .then(response=>{
        console.log(response);
            })
            .catch(function (error) {
              console.log(error);
            })
          
}


// putData to MedicalScience_API
export const PUT_COMPLAINING_PEOPLE=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}


// deleteData to MedicalScience_API
export const DELETE_COMPLAINING_PEOPLE=(url,data)=>{
  axios.put(url,data)
       .then(response=>{
        console.log('putData',response);
            })
            .catch(function (error) {
              console.log(error);
            })
}