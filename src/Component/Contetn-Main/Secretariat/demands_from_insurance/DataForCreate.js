import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'شماره',
        id:'idDfi',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام بیمه بدهکار',
        id:'dfiNameD',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'مبلغ',
        id:'dfiAmount',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ شروع',
        id:'dfiDateStart',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'datePicker',
        title:'تاریخ پایان',
        id:'dfiDtaeEnd',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'مکاتبات صورت گرفته',
        id:'dfiCorres',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'اقدامات',
        id:'dfiActions',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'dfiVist',
        required:false,
    },
]

