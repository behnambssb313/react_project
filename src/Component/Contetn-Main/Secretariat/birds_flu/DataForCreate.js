import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idBf',
        required:true,
    },
    {
        name:'input',
        type:'number',
        title:'تعداد مبتلایان',
        id:'bfNSufferers',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'محل ابتلا',
        id:'bfPSufferers',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'شماره مکاتبه',
        id:'bfCorresNumber',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'واحد مکاتبه',
        id:'bfCorrespondenceUnit',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'ارجاع',
        id:'bfReferral',
        required:false
    },
    {
        name:'textArea',
        title:'نتیجه',
        id:'bfResult',
        required:false
    },
    {
        name:'textArea',
        title:'اقدامات',
        id:'bfActions',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'bfVist',
        required:false,
    },
]

