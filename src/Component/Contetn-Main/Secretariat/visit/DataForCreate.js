import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'text',
        title:'شماره',
        id:'idVis',
        required:true,
    },
    {
        name:'input',
        type:'text',
        title:'نام بازدید کننده',
        id:'visName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی بازدید کننده',
        id:'visLastName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام همراهان',
        id:'visCompName',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام خانوادگی همراهان',
        id:'visCompFamily',
        required:false
    },
    {
        name:'datePicker',
        title:'تاریخ بازید',
        id:'visDate',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'textArea',
        title:'اقدامات',
        id:'visactions',
        required:false
    },
    {
        name:'textArea',
        title:'انتقادات',
        id:'visCritics',
        required:false
    },
    {
        name:'textArea',
        title:'مصوبات',
        id:'visApprovals',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'عنوان بازید',
        id:'visTitleVisit',
        required:false
    },
    {
        name:'input',
        type:'number',
        title:'مکان بازدید',
        id:'visPlaceVisit',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'visVist',
        required:false,
    },
]

