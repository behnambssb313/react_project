import MedicalEmergency from "./medical_emergency";
import BirdsFlu from "./birds_flu";
import ComplainingPeople from "./complaining_people";
import Suicide from "./suicide";
import DemandsFromInsurance from "./demands_from_insurance";
import CreditDeduction from "./credit_deduction";

import Companies from "./companies";
import Manuals from "./manuals";
import ProtectiveActionPlan from "./protective_action_plan";
import Visit from "./visit";

const SecretariatRoutes=[
    {
        path:'/Secretariat/Medical-Emergency',
        component:MedicalEmergency,
    },
    {
        path:'/Secretariat/Birds-Flu',
        component:BirdsFlu,
    },
    {
        path:'/Secretariat/Complaining-People',
        component:ComplainingPeople,
    },
    {
        path:'/Secretariat/Suicide',
        component:Suicide,
    },
    {
        path:'/Secretariat/Demands-From-Insurance',
        component:DemandsFromInsurance,
    },
    {
        path:'/Secretariat/Credit-Deduction',
        component:CreditDeduction,
    },
    {
        path:'/Secretariat/Companies',
        component:Companies,
    },
    {
        path:'/Secretariat/Manuals',
        component:Manuals,
    },
    {
        path:'/Secretariat/Protective-Action-Plan',
        component:ProtectiveActionPlan,
    },
    {
        path:'/Secretariat/Visit',
        component:Visit,
    },
];


export default SecretariatRoutes;
