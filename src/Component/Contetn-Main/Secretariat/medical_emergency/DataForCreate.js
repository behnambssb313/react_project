import React,{Component} from 'react';
import {Avatar } from 'antd';

export const step1=[
    {
        name:'input',
        type:'number',
        title:'تعداد بازدید کننده ها از پایگاه',
        id:'numberOfVisitors',
        required:true,
    },
    {
        name:'datePicker',
        title:'آخرین بازدید',
        id:'lastVisited',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'datePicker',
        title:'آخرین بازدید قبلی',
        id:'previousVisits',
        required:false,
        palceholder:'انتخاب کنید...'
    },
    {
        name:'input',
        type:'text',
        title:'وضعیت حضور پرسنل',
        id:'statusOfPresence',
        required:false
    },
    {
        name:'input',
        type:'text',
        title:'نام پایگاه',
        id:'baseName',
        required:false
    },
    {
        name:'textArea',
        title:'ارزیابی',
        id:'assessment',
        required:false,
    },
    {
        name:'textArea',
        title:'نواقص',
        id:'defects',
        required:false,
    },
    {
        name:'input',
        type:'number',
        title:'بازدید',
        id:'meVist',
        required:false,
    },
]

