import Dashbord from "./Contetn-Main/Dashbord";
import personnelRoutes from "./Contetn-Main/Personnel/personnelRoutes";

import Roles from "./auth/Roles";

import Users from "./auth/Users";
import ManagementUser from "./auth/Users/ManagementUser"

import AccessLevel from "./auth/AccessLevel";


const routes=[
    {
        path:'/',
        component:Dashbord,
        exact:true
    },
    {
        path:'/Settings/Roles',
        component:Roles,
    },
    {
        path:'/Settings/Users',
        component:Users,
        exact:true
    },
    {
        path:'/Settings/Users/ManagementUser',
        component:ManagementUser,
    },
    {
        path:'/Settings/AccessLevel',
        component:AccessLevel,
    },
];

let allRoutes=routes.concat(personnelRoutes)
export default allRoutes;
