import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import './App.css'
import { BrowserRouter ,Route,Link,Switch,Router} from 'react-router-dom';
import PrivateRoute from './Component/PrivateRoute';
import Auth from './Component/auth/Auth.jsx';
import Home from './Component/Home';
import axios from 'axios'


class App extends Component {
  constructor(props){
    super(props);
    this.state={
      isAuthenticated:true
    }
  }

componentDidMount(){
  let apiToken=localStorage.getItem('api_token');
  if(apiToken !== null){
    axios.get(`http://roocket.org/api/user?api_token=${apiToken}`)
          .then(response=>{
              this.setState({isAuthenticated:true})
              console.log(response);
          })
          .catch(error=>{
            this.setState({isAuthenticated:false})
          })
  }
  else{
    this.setState({isAuthenticated : false})
  }
}

handleLogin() {
  this.setState({ isAuthenticated : true});
}

logout(e){
  e.preventDefault();
  localStorage.removeItem('api_token');
  this.setState({ isAuthenticated : false});
 }
  render(){
    return(
        <Switch>
        <Route path="/Auth" render={(props) => <Auth {...props} auth={this.state.isAuthenticated} isAuth={this.handleLogin.bind(this)} />}/> 
        <PrivateRoute path="/" component={Home} auth={this.state.isAuthenticated} ddd={this.logout.bind(this)} /> 
        </Switch>
    );
}
}
  
 

export default App;
  