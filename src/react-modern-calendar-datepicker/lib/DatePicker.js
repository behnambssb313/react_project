"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Calendar = require("./Calendar");

var _DatePickerInput = _interopRequireDefault(require("./DatePickerInput"));

var _generalUtils = require("./shared/generalUtils");

var _constants = require("./shared/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var DatePicker = function DatePicker(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      formatInputText = _ref.formatInputText,
      inputPlaceholder = _ref.inputPlaceholder,
      id=_ref.id,
      inputClassName = _ref.inputClassName,
      renderInput = _ref.renderInput,
      wrapperClassName = _ref.wrapperClassName,
      calendarClassName = _ref.calendarClassName,
      calendarTodayClassName = _ref.calendarTodayClassName,
      calendarSelectedDayClassName = _ref.calendarSelectedDayClassName,
      calendarRangeStartClassName = _ref.calendarRangeStartClassName,
      calendarRangeBetweenClassName = _ref.calendarRangeBetweenClassName,
      calendarRangeEndClassName = _ref.calendarRangeEndClassName,
      disabledDays = _ref.disabledDays,
      onDisabledDayError = _ref.onDisabledDayError,
      colorPrimary = _ref.colorPrimary,
      colorPrimaryLight = _ref.colorPrimaryLight,
      minimumDate = _ref.minimumDate,
      maximumDate = _ref.maximumDate,
      selectorStartingYear = _ref.selectorStartingYear,
      selectorEndingYear = _ref.selectorEndingYear,
      isPersian = _ref.isPersian,
      shouldHighlightWeekends = _ref.shouldHighlightWeekends;
  var calendarContainerElement = (0, _react.useRef)(null);
  var dateInputElement = (0, _react.useRef)(null);
  var mousePosition = (0, _react.useRef)({});
  var shouldPreventFocus = (0, _react.useRef)(null);

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isCalendarOpen = _useState2[0],
      setCalendarVisiblity = _useState2[1];

  var handleMouseMove = function handleMouseMove(_ref2) {
    var x = _ref2.clientX,
        y = _ref2.clientY;
    mousePosition.current = {
      x: x,
      y: y
    };
  }; // get mouse live position


  (0, _react.useEffect)(function () {
    document.addEventListener('mousemove', handleMouseMove, false);
    return function () {
      document.removeEventListener('mousemove', handleMouseMove, false);
    };
  }, []); // handle input focus/blur

  (0, _react.useEffect)(function () {
    var valueType = (0, _generalUtils.getValueType)(value);
    if (valueType === _constants.TYPE_MUTLI_DATE) return; // no need to close the calendar

    var shouldCloseCalendar = valueType === _constants.TYPE_SINGLE_DATE ? !isCalendarOpen : !isCalendarOpen && value.from && value.to;
    if (shouldCloseCalendar) dateInputElement.current.blur();
  }, [value, isCalendarOpen]);

  var handleBlur = function handleBlur(e) {
    e.persist();
    if (!isCalendarOpen) return;
    var calendar = calendarContainerElement.current;
    var calendarPosition = calendar.getBoundingClientRect();

    var isInBetween = function isInBetween(position, start, end) {
      return position >= start && position <= end;
    }; // keep calendar open if clicked inside the calendar


    var isInsideCalendar = isInBetween(mousePosition.current.x, calendarPosition.left, calendarPosition.right) && isInBetween(mousePosition.current.y, calendarPosition.top, calendarPosition.bottom);

    if (isInsideCalendar) {
      shouldPreventFocus.current = true;
      e.target.focus();
      shouldPreventFocus.current = false;
      return;
    }

    setCalendarVisiblity(false);
  };

  var handleFocus = function handleFocus() {
    if (shouldPreventFocus.current) return;
    setCalendarVisiblity(true);
  }; // Keep the calendar in the screen bounds if input is near the window edges


  (0, _react.useLayoutEffect)(function () {
    if (!isCalendarOpen) return;

    var _calendarContainerEle = calendarContainerElement.current.getBoundingClientRect(),
        left = _calendarContainerEle.left,
        width = _calendarContainerEle.width;

    var clientWidth = document.documentElement.clientWidth;
    var isOverflowingFromRight = left + width > clientWidth;
    var overflowFromRightDistance = left + width - clientWidth;
    var isOverflowingFromLeft = left < 0;
    if (!isOverflowingFromRight && !isOverflowingFromLeft) return;
    var overflowFromLeftDistance = Math.abs(left);
    var rightPosition = isOverflowingFromLeft ? overflowFromLeftDistance : 0;
    var leftStyle = isOverflowingFromRight ? "calc(50% - ".concat(overflowFromRightDistance, "px)") : "calc(50% + ".concat(rightPosition, "px)");
    calendarContainerElement.current.style.left = leftStyle;
  }, [isCalendarOpen]);

  var handleCalendarChange = function handleCalendarChange(newValue) {
    var valueType = (0, _generalUtils.getValueType)(value);
    onChange(newValue);
    if (valueType === _constants.TYPE_SINGLE_DATE) setCalendarVisiblity(false);else if (valueType === _constants.TYPE_RANGE && newValue.from && newValue.to) setCalendarVisiblity(false);
  };

  return _react["default"].createElement("div", {
    className: "DatePicker ".concat(isCalendarOpen ? '-calendarOpen' : '', " ").concat(wrapperClassName)
  }, _react["default"].createElement("div", {
    ref: calendarContainerElement,
    className: "DatePicker__calendarContainer",
    "data-testid": "calendar-container"
  }, _react["default"].createElement(_Calendar.Calendar, {
    value: value,
    onChange: handleCalendarChange,
    calendarClassName: calendarClassName,
    calendarTodayClassName: calendarTodayClassName,
    calendarSelectedDayClassName: calendarSelectedDayClassName,
    calendarRangeStartClassName: calendarRangeStartClassName,
    calendarRangeBetweenClassName: calendarRangeBetweenClassName,
    calendarRangeEndClassName: calendarRangeEndClassName,
    disabledDays: disabledDays,
    colorPrimary: colorPrimary,
    colorPrimaryLight: colorPrimaryLight,
    onDisabledDayError: onDisabledDayError,
    minimumDate: minimumDate,
    maximumDate: maximumDate,
    selectorStartingYear: selectorStartingYear,
    selectorEndingYear: selectorEndingYear,
    isPersian: isPersian,
    shouldHighlightWeekends: shouldHighlightWeekends
  })), _react["default"].createElement(_DatePickerInput["default"], {
    ref: dateInputElement,
    onFocus: handleFocus,
    onBlur: handleBlur,
    formatInputText: formatInputText,
    value: value,
    inputPlaceholder: inputPlaceholder,
    id:id,
    inputClassName: inputClassName,
    renderInput: renderInput,
    isPersian: isPersian
  }));
};

DatePicker.defaultProps = {
  wrapperClassName: ''
};
DatePicker.propTypes = {
  wrapperClassName: _propTypes["default"].string
};
var _default = DatePicker;
exports["default"] = _default;