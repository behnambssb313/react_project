"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _sliderHelpers = require("../shared/sliderHelpers");

var _localeUtils = _interopRequireDefault(require("../shared/localeUtils"));

var _generalUtils = require("../shared/generalUtils");

var _constants = require("../shared/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var DaysList = function DaysList(_ref) {
  var activeDate = _ref.activeDate,
      value = _ref.value,
      monthChangeDirection = _ref.monthChangeDirection,
      onSlideChange = _ref.onSlideChange,
      disabledDays = _ref.disabledDays,
      onDisabledDayError = _ref.onDisabledDayError,
      minimumDate = _ref.minimumDate,
      maximumDate = _ref.maximumDate,
      onChange = _ref.onChange,
      isPersian = _ref.isPersian,
      calendarTodayClassName = _ref.calendarTodayClassName,
      calendarSelectedDayClassName = _ref.calendarSelectedDayClassName,
      calendarRangeStartClassName = _ref.calendarRangeStartClassName,
      calendarRangeEndClassName = _ref.calendarRangeEndClassName,
      calendarRangeBetweenClassName = _ref.calendarRangeBetweenClassName,
      shouldHighlightWeekends = _ref.shouldHighlightWeekends;
  var calendarSectionWrapper = (0, _react.useRef)(null);

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getToday = _useMemo.getToday,
      isBeforeDate = _useMemo.isBeforeDate,
      checkDayInDayRange = _useMemo.checkDayInDayRange,
      getMonthFirstWeekday = _useMemo.getMonthFirstWeekday,
      getMonthLength = _useMemo.getMonthLength,
      getLanguageDigits = _useMemo.getLanguageDigits;

  var today = getToday();
  (0, _react.useEffect)(function () {
    if (!monthChangeDirection) return;
    (0, _sliderHelpers.animateContent)({
      direction: monthChangeDirection,
      parent: calendarSectionWrapper.current
    });
  }, [monthChangeDirection]);

  var getDayRangeValue = function getDayRangeValue(day) {
    var clonedDayRange = (0, _generalUtils.deepCloneObject)(value);
    var dayRangeValue = clonedDayRange.from && clonedDayRange.to ? {
      from: null,
      to: null
    } : clonedDayRange;
    var dayRangeProp = !dayRangeValue.from ? 'from' : 'to';
    dayRangeValue[dayRangeProp] = day;
    var from = dayRangeValue.from,
        to = dayRangeValue.to; // swap from and to values if from is later than to

    if (isBeforeDate(dayRangeValue.to, dayRangeValue.from)) {
      dayRangeValue.from = to;
      dayRangeValue.to = from;
    }

    var checkIncludingDisabledDay = function checkIncludingDisabledDay(disabledDay) {
      return checkDayInDayRange({
        day: disabledDay,
        from: dayRangeValue.from,
        to: dayRangeValue.to
      });
    };

    var includingDisabledDay = disabledDays.find(checkIncludingDisabledDay);

    if (includingDisabledDay) {
      onDisabledDayError(includingDisabledDay);
      return value;
    }

    return dayRangeValue;
  };

  var getMultiDateValue = function getMultiDateValue(day) {
    var isAlreadyExisting = value.some(function (valueDay) {
      return (0, _generalUtils.isSameDay)(valueDay, day);
    });
    var addedToValue = [].concat(_toConsumableArray(value), [day]);
    var removedFromValue = value.filter(function (valueDay) {
      return !(0, _generalUtils.isSameDay)(valueDay, day);
    });
    return isAlreadyExisting ? removedFromValue : addedToValue;
  };

  var handleDayClick = function handleDayClick(day) {
    var getNewValue = function getNewValue() {
      var valueType = (0, _generalUtils.getValueType)(value);

      switch (valueType) {
        case _constants.TYPE_SINGLE_DATE:
          return day;

        case _constants.TYPE_RANGE:
          return getDayRangeValue(day);

        case _constants.TYPE_MUTLI_DATE:
          return getMultiDateValue(day);
      }
    };

    var newValue = getNewValue();
    onChange(newValue);
  };

  var isSingleDateSelected = function isSingleDateSelected(day) {
    var valueType = (0, _generalUtils.getValueType)(value);
    if (valueType === _constants.TYPE_SINGLE_DATE) return (0, _generalUtils.isSameDay)(day, value);
    if (valueType === _constants.TYPE_MUTLI_DATE) return value.some(function (valueDay) {
      return (0, _generalUtils.isSameDay)(valueDay, day);
    });
  };

  var getDayClassNames = function getDayClassNames(dayItem) {
    var isToday = (0, _generalUtils.isSameDay)(dayItem, today);
    var isSelected = isSingleDateSelected(dayItem);

    var _ref2 = value || {},
        startingDay = _ref2.from,
        endingDay = _ref2.to;

    var isStartedDayRange = (0, _generalUtils.isSameDay)(dayItem, startingDay);
    var isEndingDayRange = (0, _generalUtils.isSameDay)(dayItem, endingDay);
    var isWithinRange = checkDayInDayRange({
      day: dayItem,
      from: startingDay,
      to: endingDay
    });
    var classNames = ''.concat(isToday && !isSelected ? " -today ".concat(calendarTodayClassName) : '').concat(!dayItem.isStandard ? ' -blank' : '').concat(dayItem.isWeekend && shouldHighlightWeekends ? ' -weekend' : '').concat(isSelected ? " -selected ".concat(calendarSelectedDayClassName) : '').concat(isStartedDayRange ? " -selectedStart ".concat(calendarRangeStartClassName) : '').concat(isEndingDayRange ? " -selectedEnd ".concat(calendarRangeEndClassName) : '').concat(isWithinRange ? " -selectedBetween ".concat(calendarRangeBetweenClassName) : '').concat(dayItem.isDisabled ? ' -disabled' : '');
    return classNames;
  };

  var getViewMonthDays = function getViewMonthDays(date) {
    var prependingBlankDays = (0, _generalUtils.createUniqueRange)(getMonthFirstWeekday(date), 'starting-blank'); // all months will have an additional 7 days(week) for rendering purpose

    var appendingBlankDays = (0, _generalUtils.createUniqueRange)(7 - getMonthFirstWeekday(date), 'ending-blank');
    var standardDays = (0, _generalUtils.createUniqueRange)(getMonthLength(date)).map(function (day) {
      return _objectSpread({}, day, {
        isStandard: true,
        month: date.month,
        year: date.year
      });
    }, 'standard');
    var allDays = prependingBlankDays.concat(standardDays, appendingBlankDays);
    return allDays;
  };

  var renderMonthDays = function renderMonthDays(isInitialActiveChild) {
    var date = (0, _sliderHelpers.getSlideDate)({
      activeDate: activeDate,
      isInitialActiveChild: isInitialActiveChild,
      monthChangeDirection: monthChangeDirection,
      parent: calendarSectionWrapper.current
    });
    var allDays = getViewMonthDays(date);
    return allDays.map(function (_ref3, index) {
      var id = _ref3.id,
          day = _ref3.value,
          month = _ref3.month,
          year = _ref3.year,
          isStandard = _ref3.isStandard;
      var dayItem = {
        day: day,
        month: month,
        year: year
      };
      var isInDisabledDaysRange = disabledDays.some(function (disabledDay) {
        return (0, _generalUtils.isSameDay)(dayItem, disabledDay);
      });
      var isBeforeMinimumDate = isBeforeDate(dayItem, minimumDate);
      var isAfterMaximumDate = isBeforeDate(maximumDate, dayItem);
      var isNotInValidRange = isStandard && (isBeforeMinimumDate || isAfterMaximumDate);
      var isDisabled = isInDisabledDaysRange || isNotInValidRange;
      var isWeekend = !isPersian && index % 7 === 0 || index % 7 === 6;
      var additionalClass = getDayClassNames(_objectSpread({}, dayItem, {
        isWeekend: isWeekend,
        isStandard: isStandard,
        isDisabled: isDisabled
      }));
      return _react["default"].createElement("button", {
        tabIndex: "-1",
        key: id,
        className: "Calendar__day ".concat(isPersian ? '-persian' : '-gregorian').concat(additionalClass),
        onClick: function onClick() {
          if (isDisabled) {
            onDisabledDayError(dayItem); // good for showing error messages

            return;
          }

          handleDayClick({
            day: day,
            month: month,
            year: year
          });
        },
        disabled: !isStandard,
        type: "button"
      }, !isStandard ? '' : getLanguageDigits(day));
    });
  };

  return _react["default"].createElement("div", {
    ref: calendarSectionWrapper,
    className: "Calendar__sectionWrapper",
    "data-testid": "days-section-wrapper"
  }, _react["default"].createElement("div", {
    onAnimationEnd: function onAnimationEnd(e) {
      (0, _sliderHelpers.handleSlideAnimationEnd)(e);
      onSlideChange();
    },
    className: "Calendar__section -shown"
  }, renderMonthDays(true)), _react["default"].createElement("div", {
    onAnimationEnd: function onAnimationEnd(e) {
      (0, _sliderHelpers.handleSlideAnimationEnd)(e);
      onSlideChange();
    },
    className: "Calendar__section -hiddenNext"
  }, renderMonthDays(false)));
};

DaysList.propTypes = {
  onChange: _propTypes["default"].func,
  onDisabledDayError: _propTypes["default"].func,
  disabledDays: _propTypes["default"].arrayOf(_propTypes["default"].shape(_constants.DAY_SHAPE)),
  calendarTodayClassName: _propTypes["default"].string,
  calendarSelectedDayClassName: _propTypes["default"].string,
  calendarRangeStartClassName: _propTypes["default"].string,
  calendarRangeBetweenClassName: _propTypes["default"].string,
  calendarRangeEndClassName: _propTypes["default"].string,
  shouldHighlightWeekends: _propTypes["default"].bool
};
DaysList.defaultProps = {
  onChange: function onChange() {},
  onDisabledDayError: function onDisabledDayError() {},
  disabledDays: [],
  calendarTodayClassName: '',
  calendarSelectedDayClassName: '',
  calendarRangeStartClassName: '',
  calendarRangeBetweenClassName: '',
  calendarRangeEndClassName: '',
  shouldHighlightWeekends: false
};
var _default = DaysList;
exports["default"] = _default;