"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _generalUtils = require("../shared/generalUtils");

var _localeUtils = _interopRequireDefault(require("../shared/localeUtils"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MonthSelector = function MonthSelector(_ref) {
  var activeDate = _ref.activeDate,
      maximumDate = _ref.maximumDate,
      minimumDate = _ref.minimumDate,
      onMonthSelect = _ref.onMonthSelect,
      isOpen = _ref.isOpen,
      isPersian = _ref.isPersian;
  var monthSelector = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    var classToggleMethod = isOpen ? 'add' : 'remove';
    monthSelector.current.classList[classToggleMethod]('-open');
  }, [isOpen]);

  var _useMemo = (0, _react.useMemo)(function () {
    return (0, _localeUtils["default"])(isPersian);
  }, [isPersian]),
      getMonthNumber = _useMemo.getMonthNumber,
      isBeforeDate = _useMemo.isBeforeDate,
      monthsList = _useMemo.monthsList;

  var renderMonthSelectorItems = function renderMonthSelectorItems() {
    return monthsList.map(function (persianMonth) {
      var monthNumber = getMonthNumber(persianMonth);
      var monthDate = {
        day: 1,
        month: monthNumber,
        year: activeDate.year
      };
      var isAfterMaximumDate = maximumDate && isBeforeDate(maximumDate, _objectSpread({}, monthDate, {
        month: monthNumber
      }));
      var isBeforeMinimumDate = minimumDate && (isBeforeDate(_objectSpread({}, monthDate, {
        month: monthNumber + 1
      }), minimumDate) || (0, _generalUtils.isSameDay)(_objectSpread({}, monthDate, {
        month: monthNumber + 1
      }), minimumDate));
      return _react["default"].createElement("div", {
        key: persianMonth,
        className: "Calendar__monthSelectorItem ".concat(monthNumber === activeDate.month ? '-active' : '')
      }, _react["default"].createElement("button", {
        tabIndex: "-1",
        onClick: function onClick() {
          onMonthSelect(monthNumber);
        },
        className: "Calendar__monthSelectorItemText",
        type: "button",
        disabled: isAfterMaximumDate || isBeforeMinimumDate
      }, persianMonth));
    });
  };

  return _react["default"].createElement("div", {
    className: "Calendar__monthSelectorAnimationWrapper"
  }, _react["default"].createElement("div", {
    className: "Calendar__monthSelectorWrapper"
  }, _react["default"].createElement("div", {
    "data-testid": "month-selector",
    ref: monthSelector,
    className: "Calendar__monthSelector"
  }, renderMonthSelectorItems())));
};

var _default = MonthSelector;
exports["default"] = _default;